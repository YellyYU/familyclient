package com.familyclient.com.familyclient.action;

import java.util.ArrayList;
import java.util.List;

import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class PlanStatusAction {
    static String msg = "";
    static RequestParams params = new RequestParams();
    static Context myContext;
    private SQLHelper helper;
    private SQLiteDatabase db;
    private String username;
    private String ipAddress;

    public PlanStatusAction() {
    }

    public PlanStatusAction(Context mContext) {
        super();
        this.myContext = mContext;
        MyApp User = ((MyApp) myContext);
        username = User.getUser();
        ipAddress = User.getIpAddress();
    }

    public String ChangePlanStatus(final int planID, final int status){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("planID", planID);
            params.put("today_done", status);
            params.setUseJsonStreamer(true);
            client.post("http://"+ipAddress+":8080/updatePlan", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="Connecting to server failed!";
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);

                    int flag = myhtml.getInt("flag");
                    msg = myhtml.getString("message");
                    if(status == 1 && flag == 1){
                        //dismiss the task
                        String dbname = username+".db";
                        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                        db = helper.getWritableDatabase();
                        db.beginTransaction();  //开始事务
                        try {
                            db.execSQL("UPDATE Plan"+" set today_done = 1 where planID="+planID);
                            db.execSQL("UPDATE Plan"+" set total_done = total_done+1 where planID="+planID);
                            //以上修改plan的today_done完成

                            List<Integer> families = new ArrayList<Integer>();
                            Cursor cursor = db.query("plan_families", null, "planID = ?", new String[]{planID+""}, null, null,null);
                            if (cursor.moveToFirst()) {
                                do{
                                    families.add(cursor.getInt(cursor.getColumnIndex("familyID")));
                                }while(cursor.moveToNext());
                            }
                            for(int i=0;i<families.size();i++){
                                db.execSQL("UPDATE members"+families.get(i)+" set today_task_done = today_task_done+1 where email=\""+username+"\"");
                                db.execSQL("UPDATE members"+families.get(i)+" set week_score = week_score+1 where email=\""+username+"\"");
                            }
                            //以上修改members的today_task_done完成
                            db.setTransactionSuccessful();  //设置事务成功完成
                        } finally {
                            db.endTransaction();    //结束事务
                        }
                        db.close();
                    }
                    else if(status == 0 && flag == 1){
                        //done the task.............
                        String dbname = username+".db";
                        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                        db = helper.getWritableDatabase();
                        db.beginTransaction();  //开始事务
                        try {
                            db.execSQL("UPDATE Plan"+" set today_done = 0 where planID="+planID);
                            db.execSQL("UPDATE Plan"+" set total_done = total_done-1 where planID="+planID);
                            //以上修改plan的today_done完成

                            List<Integer> families = new ArrayList<Integer>();
                            Cursor cursor = db.query("plan_families", null, "planID = ?", new String[]{planID+""}, null, null,null);
                            if (cursor.moveToFirst()) {
                                do{
                                    families.add(cursor.getInt(cursor.getColumnIndex("familyID")));
                                }while(cursor.moveToNext());
                            }

                            for(int i=0;i<families.size();i++){
                                db.execSQL("UPDATE members"+families.get(i)+" set today_task_done = today_task_done-1 where email=\""+username+"\"");
                                db.execSQL("UPDATE members"+families.get(i)+" set week_score = week_score-1 where email=\""+username+"\"");
                            }
                            //以上修改members的today_task_done完成


                            db.setTransactionSuccessful();  //设置事务成功完成
                        } finally {
                            db.endTransaction();    //结束事务
                        }
                        db.close();
                    }
                    else{
                        Toast.makeText(myContext, msg , Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }
}
