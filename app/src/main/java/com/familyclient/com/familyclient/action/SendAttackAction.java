package com.familyclient.com.familyclient.action;

import java.sql.Timestamp;

import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class SendAttackAction {
    static String msg = "";
    static RequestParams params = new RequestParams();
    static Context myContext;
    private SQLHelper helper;
    private SQLiteDatabase db;
    private String username;
    private String ipAddress;

    public SendAttackAction() {
    }

    public SendAttackAction(Context mContext) {
        super();
        this.myContext = mContext;
        MyApp User = ((MyApp) myContext);
        username = User.getUser();
        ipAddress = User.getIpAddress();
    }

    public String Attack(final String sender, final String receiver, final String title, final String note, final int familyID){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("sender", sender);
            params.put("receiver", receiver);
            params.put("title", title);
            params.put("description", note);
            params.put("familyID", familyID);
            params.setUseJsonStreamer(true);
            client.post("http://"+ipAddress+":8080/attack", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="fail!";
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);
                    int flag = myhtml.getInt("flag");
                    String message = myhtml.getString("message");

                    if(flag == 1){
                        int attackID = myhtml.getInt("id");
                        Timestamp time = new Timestamp((Long) myhtml.get("time"));
                        //Log.i("html", time.toLocaleString());
						/*String log = (time.getYear()+1900)+"年"
								+(time.getMonth()+1)+"月"
								+time.getDate()+"日"
								+time.getHours()+"时"
								+time.getMinutes()+"分"
								+time.getSeconds()+"秒\n";*/

                        String dbname = username +".db";
                        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                        db = helper.getWritableDatabase();
                        db.beginTransaction();  //开始事务
                        try {
                            ContentValues newattack = new ContentValues();
                            newattack.put("attackID", attackID);
                            newattack.put("sender", sender);
                            newattack.put("receiver", receiver);
                            newattack.put("title", title);
                            newattack.put("note", note);
                            newattack.put("attack_time", time.toString());
                            newattack.put("familyID", familyID);
                            newattack.put("status", 0);
                            //db.execSQL("INSERT INTO person(name) values(?)",new String[]{"zhanglei"});
                            db.replace("attack", null, newattack);

                            db.execSQL("UPDATE members"+familyID+" set total_attack = total_attack+1 where email=\""+receiver+"\"");
                            db.execSQL("UPDATE members"+familyID+" set available_punish = available_punish-5 where email=\""+receiver+"\"");
                            db.setTransactionSuccessful();  //设置事务成功完成
                        } finally {
                            db.endTransaction();    //结束事务
                        }
                        db.close();
                        Toast.makeText(myContext, message , Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(myContext, message , Toast.LENGTH_SHORT).show();
                    }
                    msg="success!";
                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }
}
