package com.familyclient.com.familyclient.action;

import java.sql.Timestamp;
import java.util.List;

import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class LogRefreshAction {
    static String msg = "";
    static RequestParams params = new RequestParams();
    static Context myContext;
    private String username;
    private SQLHelper helper;
    private SQLiteDatabase db;
    private String ipAddress;

    public LogRefreshAction() {
    }

    public LogRefreshAction(Context mContext) {
        super();
        this.myContext = mContext;
        MyApp User = ((MyApp) myContext);
        username = User.getUser();
        ipAddress = User.getIpAddress();
    }

    public String Refresh(){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("email", username);
            params.setUseJsonStreamer(true);
            client.post("http://"+ipAddress+":8080/log", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="Connecting to server failed!";
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);
                    int flag_log = myhtml.getInt("flag_log");
                    int flag_attack;
                    if(myhtml.get("flag_attack") == null)
                        flag_attack = 0;
                    else
                        flag_attack = myhtml.getInt("flag_attack");

                    int flag_status = myhtml.getInt("flag_status");
                    List<JSONObject> logs = myhtml.getJSONArray("logs");
                    List<JSONObject> attacks = myhtml.getJSONArray("attacks");
                    List<JSONObject> statuses = myhtml.getJSONArray("statuses");

                    if(flag_log == 1){//need to refresh log
                        String dbname = username+".db";
                        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                        db = helper.getWritableDatabase();
                        db.beginTransaction();

                        for(int i=0;i<logs.size();i++){
                            JSONObject theLog = logs.get(i);
                            Timestamp time = new Timestamp((Long) theLog.get("time"));
                            String sender = theLog.getString("sender");
                            String type = theLog.getString("type");

                            ContentValues newlog = new ContentValues();
                            newlog.put("logtime", time.toString());
                            newlog.put("sender", sender);
                            newlog.put("receiver", username);
                            newlog.put("type", type);
                            db.replace("log", null, newlog);
                        }
                        db.setTransactionSuccessful();  //设置事务成功完成
                        db.endTransaction();    //结束事务

                        msg="1";
                    }

                    if(flag_attack ==1){//有attack需要更新
                        String dbname = username +".db";
                        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                        db = helper.getWritableDatabase();
                        db.beginTransaction();  //开始事务
                        try {
                            for(int j=0;j<attacks.size();j++){
                                JSONObject theAttack = attacks.get(j);
                                ContentValues newattack = new ContentValues();
                                newattack.put("attackID", theAttack.getInt("attackid"));
                                newattack.put("sender", theAttack.getString("sender_email"));
                                newattack.put("receiver", theAttack.getString("receiver_email"));
                                newattack.put("title", theAttack.getString("attack_title"));
                                newattack.put("note", theAttack.getString("description"));
                                Timestamp attack_time = new Timestamp((Long) theAttack.get("attack_time"));
                                newattack.put("attack_time", attack_time.toString());
                                newattack.put("familyID", theAttack.getInt("familyid"));
                                newattack.put("status", theAttack.getInt("status"));
                                db.replace("attack", null, newattack);
                            }
                            db.setTransactionSuccessful();  //设置事务成功完成
                        } finally {
                            db.endTransaction();    //结束事务
                        }
                        msg = "2";
                    }
                    if(flag_status == 1){
                        String dbname = username +".db";
                        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                        db = helper.getWritableDatabase();
                        db.beginTransaction();  //开始事务
                        try {
                            for(int j=0;j<statuses.size();j++){
                                JSONObject theStatus = statuses.get(j);
                                ContentValues newattack = new ContentValues();

                                newattack.put("status", theStatus.getInt("status"));
                                db.update("attack", newattack, "attackID = ?", new String[]{theStatus.getInt("attackID")+""});
                            }
                            db.setTransactionSuccessful();  //设置事务成功完成
                        } finally {
                            db.endTransaction();    //结束事务
                        }
                        msg = "3";
                    }


                    if(flag_log == 2&&flag_attack == 2){//nothing to refresh
                        msg = myhtml.getString("message");
                    }
                    if(flag_log == 0){
                        msg = myhtml.getString("message");
                    }
                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }
}
