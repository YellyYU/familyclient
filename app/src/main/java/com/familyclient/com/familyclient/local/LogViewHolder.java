package com.familyclient.com.familyclient.local;

import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

/**
 * Created by yu000 on 2017/10/7.
 */

public class LogViewHolder {
    public TextView content;
    public TextView statues;
    public TextView type;
    public TextView time;
}
