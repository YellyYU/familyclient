package com.familyclient.com.familyclient.fragment;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.familyclient.R;
import com.familyclient.com.familyclient.action.PlanStatusAction;
import com.familyclient.com.familyclient.activity.CreatePlanActivity;
import com.familyclient.com.familyclient.activity.ShowPlanActivity;
import com.familyclient.com.familyclient.local.MainPageViewHolder;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;

//public class MyFragment extends Fragment implements OnClickListener

public class MainFragment extends Fragment {

    private View view;
    private ListView myList;
    private static Context myContext;
    private static String username;
    private SQLHelper helper;
    private SQLiteDatabase db;
    private TextView txtZQD;
    int plan_size = 0;
    private boolean[] dos=new boolean[20];
    private static List<String> plan_name = new ArrayList<String>();
    private List<Map<String, Object>> mData = new ArrayList<Map<String, Object>>();
    private List<Integer> planIDs = new ArrayList<Integer>();
    private List<Integer> dones = new ArrayList<Integer>();
    private List<Integer> total_done = new ArrayList<Integer>();
    private List<String> start_date = new ArrayList<String>();
    private List<Integer> days = new ArrayList<Integer>();

    public MainFragment() {
        // Required empty public constructor
    }

    public static MainFragment newInstance(Context mContext) {

        myContext = mContext;
        MyApp User = ((MyApp) myContext);
        username = User.getUser();

        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(username, username);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onHiddenChanged(boolean hidden){
        super.onHiddenChanged(hidden);
        String dbname = username+".db";
        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
        db = helper.getWritableDatabase();

        //find all plans and place them in plan_name, planIDs
        plan_name.clear();
        planIDs.clear();
        dones.clear();
        total_done.clear();
        start_date.clear();
        days.clear();
        plan_size = 0;
        db.beginTransaction();
        Cursor cursor = db.query("Plan", null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                plan_name.add(cursor.getString(cursor.getColumnIndex("planname")));
                planIDs.add(cursor.getInt(cursor.getColumnIndex("planID")));
                dones.add(cursor.getInt(cursor.getColumnIndex("today_done")));
                total_done.add(cursor.getInt(cursor.getColumnIndex("total_done")));
                start_date.add(cursor.getString(cursor.getColumnIndex("begin_date")));
                int today_done = cursor.getInt(cursor.getColumnIndex("today_done"));
                if(today_done == 1){
                    dos[plan_size] = false;
                }
                else{
                    dos[plan_size] = true;
                }
                plan_size++;
            } while (cursor.moveToNext());
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    	/*Set progress bar...if we have plans*/
        for(int i=0;i<start_date.size();i++){
            SimpleDateFormat d1 = new SimpleDateFormat("yyyy-MM-dd");
            Date sDate = new Date();
            try {
                sDate = d1.parse(start_date.get(i));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date tDate = new Date();
            long diff = tDate.getTime()-sDate.getTime();
            days.add((int) (diff/(1000*60*60*24)));
        }

        ListView list=(ListView) view.findViewById(R.id.main_page_plan_list);
        MainAdapter mainadapter = new MainAdapter(myContext);
        list.setAdapter(mainadapter);
        Log.i("message", "code reaches here");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main,container,false);
        String dbname = username+".db";
        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
        db = helper.getWritableDatabase();

        //find all plans and place them in plan_name, planIDs
        plan_name.clear();
        planIDs.clear();
        dones.clear();
        db.beginTransaction();
        Cursor cursor = db.query("Plan", null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                plan_name.add(cursor.getString(cursor.getColumnIndex("planname")));
                planIDs.add(cursor.getInt(cursor.getColumnIndex("planID")));
                dones.add(cursor.getInt(cursor.getColumnIndex("today_done")));
                total_done.add(cursor.getInt(cursor.getColumnIndex("total_done")));
                start_date.add(cursor.getString(cursor.getColumnIndex("begin_date")));
                int today_done = cursor.getInt(cursor.getColumnIndex("today_done"));
                if(today_done == 1){
                    dos[plan_size] = false;
                }
                else{
                    dos[plan_size] = true;
                }
                plan_size++;
            } while (cursor.moveToNext());
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    	/*Set progress bar...if we have plans*/
        for(int i=0;i<start_date.size();i++){
            SimpleDateFormat d1 = new SimpleDateFormat("yyyy-MM-dd");
            Date sDate = new Date();
            try {
                sDate = d1.parse(start_date.get(i));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date tDate = new Date();
            long diff = tDate.getTime()-sDate.getTime();
            days.add((int) (diff/(1000*60*60*24)));
            Log.i("Parent", diff+"");
        }



        /*设置头像*/
        txtZQD = (TextView) view.findViewById(R.id.main_page_username);
        txtZQD.setText(username);
        Drawable[] drawable = txtZQD.getCompoundDrawables();
        // 数组下表0~3,依次是:左上右下
        drawable[0].setBounds(0, 0, 120, 120);
        txtZQD.setCompoundDrawables(drawable[0], null, null, null);

        Button add_plan=(Button) view.findViewById(R.id.main_page_add_plan);
        add_plan.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(myContext, CreatePlanActivity.class);
                startActivity(intent);
            }
        });

        ListView list=(ListView) view.findViewById(R.id.main_page_plan_list);


        MainAdapter mainadapter = new MainAdapter(myContext);
        list.setAdapter(mainadapter);
        return view;
    }

    @Override
    public void onResume() {
        //onResume 意味着该fragment可见
        super.onResume();

    }
    @Override
    public void onPause() {
        //onPause 意味着该fragment不可见，但是指后台的不可见，而非用户的不可见。
        super.onPause();

    }

    public class MainAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public MainAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return plan_name.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }


        @SuppressLint("InflateParams")
        public View getView(final int position, View convertView, ViewGroup parent) {

            MainPageViewHolder holder=new MainPageViewHolder();
            convertView = mInflater.inflate(R.layout.main_page_list_item, null);
            holder.complete=(ToggleButton)convertView.findViewById(R.id.main_page_list_item_togglebutton);
            //holder.complete.setChecked(dos[position]);

            holder.plan_name=(Button)convertView.findViewById(R.id.main_page_list_item_name);
            holder.plan_name.setText(plan_name.get(position));

            holder.progress=(ProgressBar)convertView.findViewById(R.id.main_page_list_item_progress);
            holder.progress.setMax(days.get(position));
            holder.progress.setProgress(total_done.get(position));

            holder.plan_name.setOnClickListener(new View.OnClickListener(){
                public void  onClick(View v){
                    Bundle bundle = new Bundle();
                    bundle.putInt("planID", planIDs.get(position));

                    Intent intent = new Intent();
                    intent.putExtra("planID", planIDs.get(position));
                    intent.setClass(myContext, ShowPlanActivity.class);
                    startActivity(intent);
                }
            });
            holder.complete.setOnCheckedChangeListener(new OnCheckedChangeListener(){
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){

                        PlanStatusAction post = new PlanStatusAction(myContext);
                        post.ChangePlanStatus(planIDs.get(position), 0);

                    }

                    else{
                        PlanStatusAction post = new PlanStatusAction(myContext);
                        post.ChangePlanStatus(planIDs.get(position), 1);
                    }
                    //onCreateView(null, null, null);
                }
            });
            return convertView;
        }
    }
}


