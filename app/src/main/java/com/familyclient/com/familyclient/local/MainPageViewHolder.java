package com.familyclient.com.familyclient.local;

import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.ToggleButton;

/**
 * Created by yu000 on 2017/10/7.
 */

public class MainPageViewHolder {
    public Button plan_name;
    public ToggleButton complete;
    public ProgressBar progress;
}
