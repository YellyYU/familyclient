package com.familyclient.com.familyclient.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.familyclient.R;
import com.familyclient.com.familyclient.action.PostPlanAction;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.AppCompatEditText;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

public class CreatePlanActivity extends Activity {
    private TextView txtZQD;
    private Spinner frequency_spinner;
    //储存checkBox控件的集合
    private List<CheckBox> checkBoxList=new ArrayList<CheckBox>();
    private LinearLayout create_plan_check_box;
    private List<JSONObject> allfamilies = new ArrayList<JSONObject>();

    //对应的所有控件
    private Button button_handin, button_cancel;
    private Button button_select_start, button_select_end;
    private String frequency;
    private EditText plan_title, plan_content;

    private int startMonth, startYear, startDay, endMonth, endYear, endDay;
    private EditText text_startDate_year, text_startDate_month, text_startDate_day, text_endDate_year, text_endDate_month, text_endDate_day;

    private SQLHelper helper;
    private SQLiteDatabase db;
    private String username;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_create_plan);

        mContext = getApplicationContext();
        MyApp User = ((MyApp) mContext);
        username = User.getUser();

        plan_title = (EditText) findViewById(R.id.create_plan_title_to_edit);
        plan_content = (EditText) findViewById(R.id.create_plan_content_to_edit);
        button_handin = (Button) findViewById(R.id.create_plan_handin);
        button_cancel = (Button) findViewById(R.id.create_plan_cancel);
        button_select_start = (Button) findViewById(R.id.create_plan_startdate_select_btn);
        button_select_end = (Button) findViewById(R.id.create_plan_enddate_select_btn);
        text_startDate_year = (EditText) findViewById(R.id.create_plan_startdate_year);
        text_startDate_month = (EditText) findViewById(R.id.create_plan_startdate_month);
        text_startDate_day = (EditText) findViewById(R.id.create_plan_startdate_date);
        text_endDate_year = (EditText) findViewById(R.id.create_plan_enddate_year);
        text_endDate_month = (EditText) findViewById(R.id.create_plan_enddate_month);
        text_endDate_day = (EditText) findViewById(R.id.create_plan_enddate_date);

        button_handin.setOnClickListener(handin_listener);
        button_cancel.setOnClickListener(cancel_listener);

        Calendar calendar = Calendar.getInstance();
        startYear = calendar.get(Calendar.YEAR);
        startMonth = calendar.get(Calendar.MONTH); // zero based
        startDay = calendar.get(Calendar.DAY_OF_MONTH);
        endYear = calendar.get(Calendar.YEAR);
        endMonth = calendar.get(Calendar.MONTH); // zero based
        endDay = calendar.get(Calendar.DAY_OF_MONTH);
        showStartDate(startYear, startMonth+1, startDay);
        showEndDate(endYear, endMonth+1, endDay);

        String dbname = username+".db";
        helper = new SQLHelper(mContext, dbname, null, SQLHelper.currVer);
        db = helper.getWritableDatabase();

        /*设置头像*/
        txtZQD = (TextView) findViewById(R.id.create_plan_username);
        Drawable[] drawable = txtZQD.getCompoundDrawables();
        // 数组下表0~3,依次是:左上右下
        drawable[0].setBounds(0, 0, 120, 120);
        txtZQD.setCompoundDrawables(drawable[0], null, null, null);

        /*设置下拉列表*/
        frequency_spinner=(Spinner) findViewById(R.id.create_plan_frequency_spinner);
        initFrequency();
        frequency_spinner.setOnItemSelectedListener(new OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent,View view,int position,long id){
                Spinner spinner=(Spinner)parent;
                frequency =(String)spinner.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        /*设置CheckBox*/
        create_plan_check_box=(LinearLayout) findViewById(R.id.create_plan_check_box);

        //对check数组赋值即可改变
        List<String> familiesname = new ArrayList<String>();
        allfamilies = getFamilies();
        for(int i=0; i<allfamilies.size(); i++){
            familiesname.add(allfamilies.get(i).getString("familyName"));
        }
        //String[] check={"家庭组1","家庭组2","家庭组3"};
        for(int i=0; i<familiesname.size(); i++){
            CheckBox checkBox=(CheckBox)View.inflate(this,R.layout.create_plan_checkbox,null);

            checkBox.setText(familiesname.get(i));
            create_plan_check_box.addView(checkBox);
            checkBoxList.add(checkBox);
        }

    }
    /*初始化frequency下拉列表*/
    private void initFrequency(){
        String[] Arr=new String[]{"一天一次","两天一次","三天一次","一周一次"};
        //1.上下文 2.列表项的资源ID 3.项资源
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,Arr);
        frequency_spinner.setAdapter(adapter);
    }

    @SuppressWarnings("deprecation")
    public void setStartDate(View view) {
        showDialog(997);
        Toast.makeText(getApplicationContext(), "ca",
                Toast.LENGTH_SHORT)
                .show();
    }
    @SuppressWarnings("deprecation")
    public void setEndDate(View view) {
        showDialog(999);
        Toast.makeText(getApplicationContext(), "ca",
                Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 997) {
            return new DatePickerDialog(this,
                    startDateListener, startYear, startMonth, startDay);
        }
        else if(id == 999){
            return new DatePickerDialog(this,
                    endDateListener, endYear, endMonth, endDay);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener startDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showStartDate(arg1, arg2+1, arg3);
                    startYear = arg1;
                    startMonth = arg2;
                    startDay = arg3;
                }
            };

    private DatePickerDialog.OnDateSetListener endDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showEndDate(arg1, arg2+1, arg3);
                    endYear = arg1;
                    endMonth = arg2;
                    endDay = arg3;
                }
            };

    private void showStartDate(int year, int month, int day) {
        text_startDate_year.setText(new StringBuilder().append(year));
        text_startDate_month.setText(new StringBuilder().append(month));
        text_startDate_day.setText(new StringBuilder().append(day));
    }

    private void showEndDate(int year, int month, int day) {
        text_endDate_year.setText(new StringBuilder().append(year));
        text_endDate_month.setText(new StringBuilder().append(month));
        text_endDate_day.setText(new StringBuilder().append(day));
    }

    private Button.OnClickListener handin_listener = new Button.OnClickListener(){
        public void  onClick(View v){
			/*获取标题、内容、起止时间*/
			/*频率和可见家庭组已经在上面赋过值了*/
            String PlanTitle = plan_title.getText().toString();
            String PlanContent = plan_content.getText().toString();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar start_date = Calendar.getInstance(), end_date = Calendar.getInstance();
            start_date.set(Calendar.YEAR, startYear);
            start_date.set(Calendar.MONTH, startMonth+1);
            start_date.set(Calendar.DAY_OF_MONTH, startDay);
            end_date.set(Calendar.YEAR, endYear);
            end_date.set(Calendar.MONTH, endMonth+1);
            end_date.set(Calendar.DAY_OF_MONTH, endDay);
            String s1 = dateFormat.format(start_date.getTime());
            String s2 = dateFormat.format(end_date.getTime());
            int freq = getfrequency(frequency);
            Set<Integer> visiblefamilies = getVisibleFamilies();

			/*String result = "first";
			for(int i=0; i<visiblefamilies.size(); i++){
				result += visiblefamilies.toString();
			}*/
            if(PlanTitle.length()>30){
                Toast.makeText(getApplicationContext(), "计划标题长于15字" , Toast.LENGTH_SHORT).show();
            }
            else if(PlanContent.length()>280){
                Toast.makeText(getApplicationContext(), "计划内容长于140字" , Toast.LENGTH_SHORT).show();
            }
            else{
                PostPlanAction post = new PostPlanAction(mContext);
                String result = post.CreatePlan(PlanTitle, s1, s2, freq, PlanContent, visiblefamilies);
			/*UpdatePlanAction post = new UpdatePlanAction(mContext);
			String result = post.UpdatePlan(3, PlanTitle, s1, s2, freq, PlanContent, visiblefamilies);*/
			/*CancelPlanAction post = new CancelPlanAction(mContext);
			String result = post.CancelPlan(4);*/
			/*CreditAction post = new CreditAction(mContext);
			String sender = "yu000013@163.com";
			String receiver = "zhanglei@qq.com";
			int familyID = 1;
			String result = post.Credit(sender, receiver);*/
			/*PunishAction post = new PunishAction(mContext);
			String sender = "yu000013@163.com";
			String receiver = "zhanglei@qq.com";
			int familyID = 1;
			String result = post.Punish(receiver, sender);*/

			/*QuitFamilyAction post = new QuitFamilyAction(mContext);
			int familyID = 3;
			String result = post.QuitFamily(familyID);*/


                Toast.makeText(getApplicationContext(), result , Toast.LENGTH_SHORT).show();

                // to main page
                Intent intent = new Intent();
                intent.setClass(CreatePlanActivity.this, MainActivity.class);
                startActivity(intent);
            }
        }
    };

    private Button.OnClickListener cancel_listener = new Button.OnClickListener(){
        public void  onClick(View v){
            Intent intent = new Intent();
            intent.setClass(CreatePlanActivity.this, MainActivity.class);
            startActivity(intent);
        }
    };

    private List<JSONObject> getFamilies(){
      List<JSONObject> visiblefamilies = new ArrayList<JSONObject>();

         /* String filename = "yu000013@163.com.txt"; // TODO: change a customized filename
        FileInputStream input;
        try {
            input = mContext.openFileInput(filename);
            int length = input.available();
            byte[] temp = new byte[length];
            StringBuilder sb = new StringBuilder("");
            int len = 0;
            while ((len = input.read(temp)) > 0) {
                sb.append(new String(temp, 0, len));
            }
            //content是把文件以string的形式取了出来
            String content = sb.toString();
            //getcontent是把content变成json格式(因为是以json格式存的)
            JSONObject getcontent = JSONObject.fromObject(content);

            visiblefamilies = (List<JSONObject>) getcontent.get("families");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }*/

        db.beginTransaction();
        Cursor cursor = db.query("familygroup", null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject jsonObj = new JSONObject();

                int familyID = cursor.getInt(cursor.getColumnIndex("familyID"));
                String family_name = cursor.getString(cursor.getColumnIndex("family_name"));
                try {
                    jsonObj.put("familyID", familyID);
                    jsonObj.put("familyName", family_name);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                visiblefamilies.add(jsonObj);
            } while (cursor.moveToNext());
        }
        db.endTransaction();
        return visiblefamilies;

    }

    private Set<Integer> getVisibleFamilies(){
        Set<Integer> result = new HashSet<Integer>();

        for(int i=0; i<checkBoxList.size(); i++){
            if(checkBoxList.get(i).isChecked()){
                result.add(Integer.parseInt(allfamilies.get(i).getString("familyID")));
            }
        }
        return result;
    }
    private int getfrequency(String frequency){
        int symbol = 0;
        if(frequency.equals("一天一次"))
            symbol = 1;
        if(frequency.equals("两天一次"))
            symbol = 2;
        if(frequency.equals("三天一次"))
            symbol = 3;
        if(frequency.equals("一周一次"))
            symbol = 7;
        return symbol;
    }

}