package com.familyclient.com.familyclient.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.familyclient.R;
import com.familyclient.com.familyclient.action.SendAttackAction;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class CreatePunishmentActivity extends Activity {
    private TextView txtZQD;
    private Spinner choices;
    String[] content=new String[]{"自定义","发红包","洗碗","扫地","唱歌"};
    String[] text={"","今晚，猎个痛快","我将带头冲锋..............................","外服锤石都是怪物","啊里啊呀痛"};
    private Button cancel;
    private Button handin;
    private Context mContext;
    private String username;
    private SQLHelper helper;
    private SQLiteDatabase db;
    private EditText title, tmp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_punishment);
        mContext = getApplicationContext();
        MyApp User = ((MyApp) mContext);
        username = User.getUser();

        cancel = (Button) findViewById(R.id.create_punishment_cancel);
        handin = (Button) findViewById(R.id.create_punishment_handin);
        cancel.setOnClickListener(cancel_listener);
        handin.setOnClickListener(handin_listener);

        /*设置头像*/
        txtZQD = (TextView) findViewById(R.id.create_punishment_username);
        Drawable[] drawable = txtZQD.getCompoundDrawables();
        // 数组下表0~3,依次是:左上右下
        drawable[0].setBounds(0, 0, 120, 120);
        txtZQD.setCompoundDrawables(drawable[0], null, null, null);

        /*设置content属性*/
        tmp=(EditText)findViewById(R.id.create_punishment_content_to_edit);
        tmp.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        tmp.setGravity(Gravity.TOP);
        tmp.setSingleLine(false);
        tmp.setHorizontallyScrolling(false);

        /*设置下拉列表*/
        choices=(Spinner) findViewById(R.id.create_punishment_choices);
        initChoices();
        choices.setOnItemSelectedListener(new OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent,View view,int position,long id){
                Spinner spinner=(Spinner)parent;
                String select=(String)spinner.getItemAtPosition(position);
                Toast.makeText(getApplicationContext(), select,
                        Toast.LENGTH_SHORT).show();

                title=(EditText)findViewById(R.id.create_punishment_title_to_edit);

                if(position!=0) title.setText(content[position]);
                else title.setText("");
                tmp.setText(text[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        /*Listener没有写，确定和取消按钮的*/
    }
    /*初始化frequency下拉列表*/
    private void initChoices(){

        //1.上下文 2.列表项的资源ID 3.项资源
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,content);
        choices.setAdapter(adapter);
    }

    private Button.OnClickListener cancel_listener = new Button.OnClickListener(){
        public void  onClick(View v){
            Intent intent = new Intent();
            intent.setClass(CreatePunishmentActivity.this, FamilyGroupActivity.class);
            startActivity(intent);
        }
    };

    private Button.OnClickListener handin_listener = new Button.OnClickListener(){
        public void  onClick(View v){
            SendAttackAction post = new SendAttackAction(mContext);
            int familyID = 1;
            String sender = username;
            String attack_title = title.getText().toString();
            String attack_note = tmp.getText().toString();
			/*写死receiver*/
            String receiver = "zhanglei@163.com";
            if(attack_title.length() > 30){
                Toast.makeText(getApplicationContext(), "标题不得大于15字", Toast.LENGTH_SHORT).show();
            }
            else if(attack_note.length() > 280){
                Toast.makeText(getApplicationContext(), "内容不得大于140字", Toast.LENGTH_SHORT).show();
            }
            else{
                String result;
                result = post.Attack(sender, receiver, attack_title, attack_note, familyID);
                Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
                //接下来应该设置跳转或提示
            }
        }
    };
}
