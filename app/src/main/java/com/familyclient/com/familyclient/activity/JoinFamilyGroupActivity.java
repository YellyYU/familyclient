package com.familyclient.com.familyclient.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.view.View;

import com.familyclient.R;
import com.familyclient.com.familyclient.action.JoinFamilyAction;
import com.familyclient.com.familyclient.local.MyApp;

public class JoinFamilyGroupActivity extends Activity {
    private EditText familyID, password;
    private Context mContext;
    private int familyAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_family_group);
        mContext = getApplicationContext();
        MyApp User = ((MyApp) mContext);
        familyAmount = User.getFamilyAmount();
        familyID = (EditText) findViewById(R.id.add_family_group_nickname);
        password = (EditText) findViewById(R.id.add_family_group_password);

	    /*设置加入与取消按钮*/
        Button create=(Button)findViewById(R.id.add_family_group_participate);
        create.setOnClickListener(new View.OnClickListener(){
            public void  onClick(View v){
                String pwdValue = password.getText().toString();
                if(familyAmount>4){
                    Toast.makeText(JoinFamilyGroupActivity.this, "您的家庭组数量已达到5个，无法再加入!", Toast.LENGTH_SHORT).show();
                }
                else if(pwdValue.length()<8||pwdValue.length()>28){
                    Toast.makeText(JoinFamilyGroupActivity.this, "密码长度应在8—28之间!", Toast.LENGTH_SHORT).show();
                }
                else{
                    JoinFamilyAction post = new JoinFamilyAction(mContext);
                    String result = post.JoinFamily(Integer.parseInt(familyID.getText().toString()), password.getText().toString());
                    Toast.makeText(getApplicationContext(), result , Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button cancel=(Button)findViewById(R.id.add_family_group_cancel);
        cancel.setOnClickListener(new View.OnClickListener(){
            public void  onClick(View v){
				/*取消按钮，返回家庭组页面？还有其他操作可以继续加，*/
                Intent intent = new Intent();
                intent.setClass(JoinFamilyGroupActivity.this, FamilyGroupActivity.class);
                startActivity(intent);
            }
        });

    }
}
