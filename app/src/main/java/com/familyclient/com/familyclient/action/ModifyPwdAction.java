package com.familyclient.com.familyclient.action;

import com.familyclient.com.familyclient.activity.MainActivity;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class ModifyPwdAction {
    static String msg = "";
    static RequestParams params = new RequestParams();
    static Context myContext;
    private String username;
    private SQLHelper helper;
    private SQLiteDatabase db;
    private String ipAddress;

    public ModifyPwdAction() {
    }

    public ModifyPwdAction(Context mContext) {
        super();
        this.myContext = mContext;
        MyApp User = ((MyApp) myContext);
        username = User.getUser();
        ipAddress = User.getIpAddress();
        String dbname = username+".db";
        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
        db = helper.getWritableDatabase();
    }

    public String Modify(final String new_pwd){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("email", username);
            params.put("new_pwd", new_pwd);
            params.setUseJsonStreamer(true);
            client.post("http://"+ipAddress+":8080/pwd", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="Connectting to server failed!";
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);

                    int flag = myhtml.getInt("flag");
                    if(flag == 1){
                        db.beginTransaction();
                        ContentValues updateuser = new ContentValues();
                        updateuser.put("password", new_pwd);
                        db.update("User", updateuser, "email = ?", new String[]{username});
                        db.endTransaction();
                        //After update the new_pwd, we jump back to the page
                        Toast.makeText(myContext, "密码修改成功", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent();
                        intent.setClass(myContext, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        myContext.startActivity(intent);
                        msg = "succeed!";
                    }
                    else{
                        Toast.makeText(myContext, "密码保存失败，请重试", Toast.LENGTH_SHORT).show();
                        msg = "fail!";
                    }
                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }
}
