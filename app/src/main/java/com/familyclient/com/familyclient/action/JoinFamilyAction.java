package com.familyclient.com.familyclient.action;

import java.util.List;

import com.familyclient.com.familyclient.activity.MainActivity;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class JoinFamilyAction {
    static String msg = "";
    static RequestParams params = new RequestParams();
    static Context myContext;
    private SQLHelper helper;
    private SQLiteDatabase db;
    private String username;
    private String ipAddress;

    public JoinFamilyAction() {
    }

    public JoinFamilyAction(Context mContext) {
        super();
        this.myContext = mContext;
        MyApp User = ((MyApp) myContext);
        username = User.getUser();
        ipAddress = User.getIpAddress();
    }

    public String JoinFamily(final int familyID, final String familyPassword){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("email", username);
            params.put("familyID", familyID);
            params.put("familyPassword", familyPassword);
            params.setUseJsonStreamer(true);
            client.post("http://"+ipAddress+":8080/joinFamily", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="fail!";
                    Toast.makeText(myContext, "Connecting to server failed!" , Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);

                    int flag = myhtml.getInt("flag");
                    if(flag == 1){
                        JSONObject newFamily = myhtml.getJSONObject("family");
                        List<JSONObject> members = newFamily.getJSONArray("members");
                        String familyName = newFamily.getString("familyName");

                        String dbname = username+".db";
                        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                        db = helper.getWritableDatabase();
                        db.beginTransaction();

                        ContentValues newfamily = new ContentValues();
                        newfamily.put("familyID", familyID);
                        newfamily.put("family_name", familyName);
                        db.insert("familygroup", null, newfamily);

                        String filename = "members"+familyID;
                        db.execSQL("DROP TABLE IF EXISTS "+filename);
                        db.execSQL("CREATE TABLE "+filename+"(email VARCHAR(50) PRIMARY KEY,username VARCHAR(1024), total_plan INTEGER, today_task_done INTEGER, total_punish INTEGER, total_credit INTEGER, total_attack INTEGER, available_credit INTEGER, available_punish INTEGER, week_score INTEGER)");

                        for(int j=0;j<members.size();j++){
                            ContentValues member_insert = new ContentValues();
                            JSONObject theMember = members.get(j);
                            member_insert.put("email", theMember.getString("email"));
                            member_insert.put("username", theMember.getString("username"));
                            member_insert.put("total_plan", theMember.getInt("total_plan"));
                            member_insert.put("today_task_done", theMember.getInt("today_task_done"));
                            member_insert.put("total_punish", theMember.getInt("total_punish"));
                            member_insert.put("total_credit", theMember.getInt("total_credit"));
                            member_insert.put("total_attack", theMember.getInt("total_attack"));
                            member_insert.put("available_credit", theMember.getInt("available_credit"));
                            member_insert.put("available_punish", theMember.getInt("available_punish"));
                            member_insert.put("week_score", theMember.getInt("week_score"));
                            db.insert(filename, null, member_insert);
                        }
                        db.setTransactionSuccessful();
                        db.endTransaction();
                        db.close();
                        msg="success!";
                        Toast.makeText(myContext, "Join succeeded!" , Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent();
                        intent.setClass(myContext, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        myContext.startActivity(intent);
                    }
                    else{
                        Toast.makeText(myContext, myhtml.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }
}
