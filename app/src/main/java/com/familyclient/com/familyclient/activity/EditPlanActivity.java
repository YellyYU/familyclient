package com.familyclient.com.familyclient.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import com.familyclient.R;
import com.familyclient.com.familyclient.action.UpdatePlanAction;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import net.sf.json.JSONObject;

public class EditPlanActivity extends Activity {
    private TextView txtZQD;
    private Spinner frequency_spinner;
    private String username;
    //储存checkBox控件的集合
    private List<CheckBox> checkBoxList=new ArrayList<CheckBox>();
    private LinearLayout edit_plan_check_box;
    private List<JSONObject> allfamilies = new ArrayList<JSONObject>();

    //对应的所有控件
    private Button button_handin, button_cancel;
    private String plan_frequency_str;
    private TextView plan_title;
    private EditText plan_content;

    private int planID, plan_progress_val, plan_frequency_rep;
    private String plan_name;
    private int startMonth, startYear, startDay, endMonth, endYear, endDay;
    private ArrayList<Integer> visible_family_arr;
    private EditText text_startDate_year, text_startDate_month, text_startDate_day, text_endDate_year, text_endDate_month, text_endDate_day;

    private static final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    private static final String[] FreqArr=new String[]{"一天一次","两天一次","三天一次","一周一次"};

    private Context mContext;

    private SQLHelper helper;
    private SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_plan);
        mContext = getApplicationContext();
        MyApp User = ((MyApp) mContext);
        username = User.getUser();

        plan_title = (TextView) findViewById(R.id.edit_plan_title);
        plan_content = (EditText) findViewById(R.id.edit_plan_content_to_edit);

        button_handin = (Button) findViewById(R.id.edit_plan_handin);
        button_cancel = (Button) findViewById(R.id.edit_plan_cancel);
        button_handin.setOnClickListener(handin_listener);
        button_cancel.setOnClickListener(cancel_listener);

        text_startDate_year = (EditText) findViewById(R.id.edit_plan_startdate_year);
        text_startDate_month = (EditText) findViewById(R.id.edit_plan_startdate_month);
        text_startDate_day = (EditText) findViewById(R.id.edit_plan_startdate_date);
        text_endDate_year = (EditText) findViewById(R.id.edit_plan_enddate_year);
        text_endDate_month = (EditText) findViewById(R.id.edit_plan_enddate_month);
        text_endDate_day = (EditText) findViewById(R.id.edit_plan_enddate_date);


        //新页面接收数据
        Bundle bundle = this.getIntent().getExtras();

        //接收planID值
        planID = bundle.getInt("planID");
        plan_name = bundle.getString("planname");
        plan_progress_val = bundle.getInt("progress");

        plan_title.setText(plan_name);
        plan_content.setText(bundle.getString("note"));

        Date end_date=null, start_date = null;
        try {
            start_date = df.parse(bundle.getString("start_date"));
            end_date = df.parse(bundle.getString("end_date"));

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Calendar start_calendar = Calendar.getInstance();
        start_calendar.setTime(start_date);
        startYear = start_calendar.get(Calendar.YEAR);
        startMonth = start_calendar.get(Calendar.MONTH); // zero based
        startDay = start_calendar.get(Calendar.DAY_OF_MONTH);
        showStartDate(startYear, startMonth+1, startDay);

        Calendar end_calendar = Calendar.getInstance();
        end_calendar.setTime(end_date);
        endYear = end_calendar.get(Calendar.YEAR);
        endMonth = end_calendar.get(Calendar.MONTH); // zero based
        endDay = end_calendar.get(Calendar.DAY_OF_MONTH);
        showEndDate(endYear, endMonth+1, endDay);

        /*设置头像*/
        txtZQD = (TextView) findViewById(R.id.edit_plan_username);
        Drawable[] drawable = txtZQD.getCompoundDrawables();
        // 数组下表0~3,依次是:左上右下
        drawable[0].setBounds(0, 0, 120, 120);
        txtZQD.setCompoundDrawables(drawable[0], null, null, null);

        /*设置下拉列表*/
        plan_frequency_rep = bundle.getInt("frequency");
        frequency_spinner=(Spinner) findViewById(R.id.edit_plan_frequency_spinner);
        initFrequency();

        frequency_spinner.setOnItemSelectedListener(new OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent,View view,int position,long id){
                Spinner spinner=(Spinner)parent;
                plan_frequency_str =(String)spinner.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        /*设置CheckBox*/
        edit_plan_check_box=(LinearLayout) findViewById(R.id.edit_plan_check_box);

        //对check数组赋值即可改变

        visible_family_arr = bundle.getIntegerArrayList("visiblefamilies");
        allfamilies = getAllFamily();
        for(int i=0; i<allfamilies.size(); i++){
            CheckBox checkBox=(CheckBox)View.inflate(this,R.layout.edit_plan_checkbox,null);

            JSONObject oneFamily = allfamilies.get(i);
            checkBox.setText(oneFamily.getString("familyName"));
//            boolean checked = visible_family_arr.contains(oneFamily.getInt("familyID"));
//            checkBox.setChecked(checked);

            edit_plan_check_box.addView(checkBox);
            checkBoxList.add(checkBox);
        }

    }

    // correspond to order of frequency array.
    private int findFrequencyPos(int frequency){
        switch (frequency){
            case 1: return 1;
            case 2: return 2;
            case 3: return 3;
            case 7: return 4;
            default: System.err.println("EditPlanActivity, undefined frequency representation: " + frequency);
                return 1;
        }
    }

    /*初始化frequency下拉列表*/
    private void initFrequency(){
        //1.上下文 2.列表项的资源ID 3.项资源
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, FreqArr);
        frequency_spinner.setAdapter(adapter);
        frequency_spinner.setSelection(findFrequencyPos(plan_frequency_rep));
    }

    @SuppressWarnings("deprecation")
    public void setStartDate(View view) {
        showDialog(997);
        Toast.makeText(getApplicationContext(), "ca",
                Toast.LENGTH_SHORT)
                .show();
    }
    @SuppressWarnings("deprecation")
    public void setEndDate(View view) {
        showDialog(999);
        Toast.makeText(getApplicationContext(), "ca",
                Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 997) {
            return new DatePickerDialog(this,
                    startDateListener, startYear, startMonth, startDay);
        }
        else if(id == 999){
            return new DatePickerDialog(this,
                    endDateListener, endYear, endMonth, endDay);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener startDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showStartDate(arg1, arg2+1, arg3);
                    startYear = arg1;
                    startMonth = arg2;
                    startDay = arg3;
                }
            };

    private DatePickerDialog.OnDateSetListener endDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showEndDate(arg1, arg2+1, arg3);
                    endYear = arg1;
                    endMonth = arg2;
                    endDay = arg3;
                }
            };

    private void showStartDate(int year, int month, int day) {
        text_startDate_year.setText(new StringBuilder().append(year));
        text_startDate_month.setText(new StringBuilder().append(month));
        text_startDate_day.setText(new StringBuilder().append(day));
    }

    private void showEndDate(int year, int month, int day) {
        text_endDate_year.setText(new StringBuilder().append(year));
        text_endDate_month.setText(new StringBuilder().append(month));
        text_endDate_day.setText(new StringBuilder().append(day));
    }

    private Button.OnClickListener handin_listener = new Button.OnClickListener(){
        public void  onClick(View v){
			/*获取标题、内容、起止时间*/
			/*频率和可见家庭组已经在上面赋过值了*/
            String PlanTitle = plan_title.getText().toString();
            String PlanContent = plan_content.getText().toString();

            Calendar start_date = Calendar.getInstance(), end_date = Calendar.getInstance();
            start_date.set(Calendar.YEAR, startYear);
            start_date.set(Calendar.MONTH, startMonth);
            start_date.set(Calendar.DAY_OF_MONTH, startDay);
            end_date.set(Calendar.YEAR, endYear);
            end_date.set(Calendar.MONTH, endMonth);
            end_date.set(Calendar.DAY_OF_MONTH, endDay);
            String s1 = df.format(start_date.getTime());
            String s2 = df.format(end_date.getTime());

            plan_frequency_rep = getfrequency(plan_frequency_str);
            Set<Integer> visiblefamilies = getVisibleFamilies();
            if(PlanTitle.length()>30){
                Toast.makeText(getApplicationContext(), "计划标题不得长于15字" , Toast.LENGTH_SHORT).show();
            }
            else if(PlanContent.length()>280){
                Toast.makeText(getApplicationContext(), "计划内容不得长于140字" , Toast.LENGTH_SHORT).show();
            }
            else{
                UpdatePlanAction post = new UpdatePlanAction(mContext);
                String result = post.UpdatePlan(planID, PlanTitle, s1, s2, plan_frequency_rep, PlanContent, visiblefamilies);

                Toast.makeText(getApplicationContext(), result , Toast.LENGTH_SHORT).show();

                // go back to showPlan activity
                Intent intent = new Intent();
                intent.putExtra("planID", planID);
                intent.putExtra("planname", plan_name);
                intent.putExtra("note", plan_content.getText().toString());
                intent.putExtra("progress", plan_progress_val);

                intent.putExtra("begin_date", ""+s1);
                intent.putExtra("end_date", ""+s2);
                intent.putExtra("frequency", plan_frequency_rep);
                intent.setClass(EditPlanActivity.this, ShowPlanActivity.class);
                startActivity(intent);
            }
        }
    };

    private Button.OnClickListener cancel_listener = new Button.OnClickListener(){
        public void  onClick(View v){
            Intent intent = new Intent();
            intent.setClass(EditPlanActivity.this, MainActivity.class);
            startActivity(intent);
        }
    };


    private Set<Integer> getVisibleFamilies(){
        Set<Integer> result = new HashSet<Integer>();

        for(int i=0; i<checkBoxList.size(); i++){
            if(checkBoxList.get(i).isChecked()){
                result.add(Integer.parseInt(allfamilies.get(i).getString("familyID")));
            }
        }
        return result;
    }

    private List<JSONObject> getAllFamily(){
        List<JSONObject> result = new ArrayList<JSONObject>();
        String dbname = username+".db";
        helper = new SQLHelper(mContext, dbname, null, SQLHelper.currVer);
        db = helper.getWritableDatabase();
        Cursor cursor = db.query("familygroup", null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do{JSONObject theFamily = new JSONObject();
                theFamily.put("familyName", cursor.getString(cursor.getColumnIndex("family_name")));
                theFamily.put("familyID", cursor.getInt(cursor.getColumnIndex("familyID")));
                result.add(theFamily);
            }while(cursor.moveToNext());
        }
        return result;
    }

    private int getfrequency(String frequency){
        int symbol = 0;
        if(frequency.equals("一天一次"))
            symbol = 1;
        if(frequency.equals("两天一次"))
            symbol = 2;
        if(frequency.equals("三天一次"))
            symbol = 3;
        if(frequency.equals("一周一次"))
            symbol = 7;
        return symbol;
    }

}