package com.familyclient.com.familyclient.action;

import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class AttackHandledAction {
    static String msg = "";
    static RequestParams params = new RequestParams();
    static Context myContext;
    private SQLHelper helper;
    private SQLiteDatabase db;
    private String username;
    private String ipAddress;

    public AttackHandledAction() {
    }

    public AttackHandledAction(Context mContext) {
        super();
        this.myContext = mContext;
        MyApp User = ((MyApp) myContext);
        username = User.getUser();
        ipAddress = User.getIpAddress();
    }

    public String attackDone(final int attackID){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("attackID", attackID);
            params.setUseJsonStreamer(true);
            client.post("http://"+ipAddress+":8080/attackDone", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="fail!";
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);
                    int flag = myhtml.getInt("flag");
                    String message = myhtml.getString("message");

                    if(flag == 1){
                        String dbname = username+".db";
                        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                        db = helper.getWritableDatabase();
                        db.beginTransaction();  //开始事务
                        try {
                            ContentValues status_update = new ContentValues();
                            status_update.put("status", 3);
                            db.update("attack", status_update, "attackID = ?", new String[]{attackID+""});
                            db.setTransactionSuccessful();  //设置事务成功完成
                        } finally {
                            db.endTransaction();    //结束事务
                        }
                        db.close();
                        Toast.makeText(myContext, message, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(myContext, message, Toast.LENGTH_SHORT).show();
                    }
                    msg="success!";
                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }

    public String attackResist(final int attackID){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("attackID", attackID);
            params.setUseJsonStreamer(true);
            client.post("http://"+ipAddress+":8080/attackResist", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="fail!";
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    msg = "success";
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);
                    int flag = myhtml.getInt("flag");
                    String message = myhtml.getString("message");

                    if(flag == 1){
                        String dbname = username+".db";
                        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                        db = helper.getWritableDatabase();
                        db.beginTransaction();  //开始事务
                        try {
                            ContentValues status_update = new ContentValues();
                            status_update.put("status", 5);
                            db.update("attack", status_update, "attackID = ?", new String[]{attackID+""});
                            db.setTransactionSuccessful();  //设置事务成功完成
                        } finally {
                            db.endTransaction();    //结束事务
                        }
                        db.close();
                        Toast.makeText(myContext, message, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(myContext, message, Toast.LENGTH_SHORT).show();
                    }
                    msg="success!";
                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }
}
