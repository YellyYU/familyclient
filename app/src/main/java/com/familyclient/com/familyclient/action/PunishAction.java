package com.familyclient.com.familyclient.action;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;

import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class PunishAction {
    static String msg = "";
    static RequestParams params = new RequestParams();
    static Context myContext;
    private String username;
    private SQLHelper helper;
    private SQLiteDatabase db;
    private String ipAddress;

    public PunishAction() {
    }

    public PunishAction(Context mContext) {
        super();
        this.myContext = mContext;
        MyApp User = ((MyApp) myContext);
        username = User.getUser();
        ipAddress = User.getIpAddress();
    }

    public String Punish(final String sender, final String receiver, final int familyID){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("sender", sender);
            params.put("receiver", receiver);
            params.put("familyID", familyID);
            params.setUseJsonStreamer(true);
            client.post("http://"+ipAddress+":8080/punish", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="fail!";
                    Toast.makeText(myContext, "Connecting to server failed!" , Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);
                    int flag = myhtml.getInt("flag");
                    String message = myhtml.getString("message");

                    if(flag == 1){
                        Timestamp time = new Timestamp((Long) myhtml.get("time"));
                        Log.i("html", time.toLocaleString());
                        String log = sender+" "
                                +"punish"+" "
                                +receiver+" "
                                +(time.getYear()+1900)+"年"
                                +(time.getMonth()+1)+"月"
                                +time.getDate()+"日"
                                +time.getHours()+"时"
                                +time.getMinutes()+"分"
                                +time.getSeconds()+"秒\n";
                        //如果成功，则修改本地文件中个人用户的奖惩
                        String filename = username + ".credit&punish.txt";
                        FileOutputStream output;
                        try {

                            //写本地文件
                            output = myContext.openFileOutput(filename, Context.MODE_APPEND);
                            output.write(log.getBytes());  //将String字符串以字节流的形式写入到输出流中
                            output.close();         //关闭输出流
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e){
                            e.printStackTrace();
                        }

                        String dbname = username+".db";
                        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                        db = helper.getWritableDatabase();
                        db.beginTransaction();
                        ContentValues newlog = new ContentValues();
                        newlog.put("logtime", time.toString());
                        newlog.put("sender", username);
                        newlog.put("receiver", receiver);
                        newlog.put("type", "punish");
                        db.insert("log", null, newlog);

                        db.execSQL("UPDATE members"+familyID+" set total_punish = total_punish+1 where email=\""+receiver+"\"");
                        db.execSQL("UPDATE members"+familyID+" set available_punish = available_punish+1 where email=\""+receiver+"\"");
                        db.setTransactionSuccessful();
                        db.endTransaction();
                        db.close();
                    }
                    else{
                        Toast.makeText(myContext, message , Toast.LENGTH_SHORT).show();
                    }
                    msg="success!";
                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }
}
