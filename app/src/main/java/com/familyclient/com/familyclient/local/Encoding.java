package com.familyclient.com.familyclient.local;

/**
 * Created by yu000 on 2017/10/7.
 */
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Encoding {
    private static String getMD5(String content) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(content.getBytes());
            return getHashString(digest);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getHashString(MessageDigest digest) {
        StringBuilder builder = new StringBuilder();
        for (byte b : digest.digest()) {
            builder.append(Integer.toHexString((b >> 4) & 0xf));
            builder.append(Integer.toHexString(b & 0xf));
        }
        return builder.toString();
    }

    public static String getMD5x99(String content){
        String s1 = content;
        for(int i = 0;i < 99;i++){
            s1 = getMD5(s1);
        }
        return s1;
    }
}
