package com.familyclient.com.familyclient.action;


import java.util.ArrayList;
import java.util.List;

import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class AttackRefreshAction {
    static String msg = "";
    static RequestParams params = new RequestParams();
    static Context myContext;
    private SQLHelper helper;
    private SQLiteDatabase db;
    private String username;
    private String ipAddress;

    public AttackRefreshAction() {
    }

    public AttackRefreshAction(Context mContext) {
        super();
        this.myContext = mContext;
        MyApp User = ((MyApp) myContext);
        username = User.getUser();
        ipAddress = User.getIpAddress();
    }

    public String refresh(){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("email", username);
            params.setUseJsonStreamer(true);
            client.post("http://"+ipAddress+":8080/attackPage", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="Connecting to server failed!";
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);
                    int flag = myhtml.getInt("flag");
                    List<JSONObject> attacks_update = new ArrayList<JSONObject>();

                    if(flag == 1){
                        String dbname = username+".db";
                        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                        db = helper.getWritableDatabase();
                        db.beginTransaction();  //开始事务
                        attacks_update = myhtml.getJSONArray("attacks");

                        for(int i=0;i<attacks_update.size();i++){
                            JSONObject theAttack = attacks_update.get(i);
                            int attackID = theAttack.getInt("attackid");
                            int status = theAttack.getInt("status");
                            String sender = theAttack.getString("sender_email");
                            String receiver = theAttack.getString("receiver_email");
                            String title = theAttack.getString("attack_title");
                            String note = theAttack.getString("description");
                            String attack_time = theAttack.getString("attack_time");
                            ContentValues theUpdate = new ContentValues();
                            theUpdate.put("status", status);
                            theUpdate.put("sender", sender);
                            theUpdate.put("receiver", receiver);
                            theUpdate.put("title", title);
                            theUpdate.put("note", note);
                            theUpdate.put("attack_time", attack_time);
                            db.update("attack", theUpdate, "attackID = ?", new String[]{attackID+""});
                        }
                        db.setTransactionSuccessful();
                        db.endTransaction();
                        msg="1";
                    }
                    else if(flag == 2){
                        msg = myhtml.getString("message");
                    }
                    else{
                        msg = myhtml.getString("message");
                    }
                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }
}
