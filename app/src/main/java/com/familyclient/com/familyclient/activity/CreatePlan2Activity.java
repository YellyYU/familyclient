package com.familyclient.com.familyclient.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.familyclient.R;

public class CreatePlan2Activity extends Activity {
    private Button mybutton;
    private ImageView myimage;
    private CheckBox mybox4, mybox5, mybox6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_plan2);
        setTitle("EditTextActivity");
        //新页面接收数据
        Bundle bundle = this.getIntent().getExtras();
        //接收name值
        String message1 = bundle.getString("message1");
        String message2 = bundle.getString("message2");
        Log.i("获取到的message1为",message1);
        Log.i("获取到的message2为",message2);

        RelativeLayout mainLayout = (RelativeLayout)findViewById(R.id.layout_root);
        TextView text = new TextView(this);
        text.setText("Hello World");
        text.setId(1);
        //text.setLayoutParams(new ViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
        RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp1.addRule(RelativeLayout.ALIGN_TOP,R.id.button);
        //lp1.addRule(RelativeLayout.ALIGN_LEFT,R.id.button);

        mybox4 = new CheckBox(this);
        mybox4.setText("first box");
        mybox4.setId(4);

        RelativeLayout.LayoutParams lp4 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp4.addRule(RelativeLayout.ALIGN_TOP, 1);
        //lp4.addRule(RelativeLayout.ALIGN_LEFT, 1);

        mybox5 = new CheckBox(this);
        mybox5.setText("second box");
        mybox5.setId(5);

        RelativeLayout.LayoutParams lp5 = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp5.addRule(RelativeLayout.ALIGN_TOP, 4);
        lp5.addRule(RelativeLayout.ALIGN_LEFT, 4);
        /*lp1.leftMargin = 30;
        lp1.topMargin = 100;*/
        mainLayout.addView(text, lp1);
        mainLayout.addView(mybox4, lp4);
        mainLayout.addView(mybox5, lp5);
        //setContentView(mainLayout);

        find_and_modify_text_view();
        //mybutton = (Button) findViewById(R.id.button1);
		/*以上几行都是不变的，可以增加不能删除*/
		/*mybutton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				Toast.makeText(getApplicationContext(),"You clicked a button.", Toast.LENGTH_SHORT).show();
			}
		});*/
    }

    private void find_and_modify_text_view(){
        mybutton = (Button) findViewById(R.id.button);
        myimage = (ImageView) findViewById(R.id.imageView1);
        mybutton.setOnClickListener(get_listener);
    }

    private Button.OnClickListener get_listener = new Button.OnClickListener(){
        public void  onClick(View v){
            EditText edit_text = (EditText) findViewById(R.id.edit_text);
            CharSequence edit_text_value = edit_text.getText();
            setTitle("EditText的值："+edit_text_value);
            //打开相机
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            //取出相机的照片bitmap格式
			/*// create a file to save the image
            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);*/
            //long time = Calendar.getInstance().getTimeInMillis();
			/*intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(Environment
			.getExternalStorageDirectory().getAbsolutePath()+"/STORAGE/emulated/0/DCIM/Camera/", time + ".jpg")));*/

            startActivityForResult(intent, 0);
            //onActivityForResut();


        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bundle extras = data.getExtras();
        Bitmap bitmap = (Bitmap) extras.get("data");
        myimage.setImageBitmap(bitmap);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
