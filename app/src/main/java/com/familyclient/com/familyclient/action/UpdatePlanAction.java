package com.familyclient.com.familyclient.action;

import java.util.Iterator;
import java.util.Set;

import com.familyclient.com.familyclient.activity.MainActivity;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class UpdatePlanAction {
    static String msg = "";
    static RequestParams params = new RequestParams();
    static Context myContext;
    private SQLHelper helper;
    private SQLiteDatabase db;
    private String username;
    private String ipAddress;


    public UpdatePlanAction() {
    }

    public UpdatePlanAction(Context mContext) {
        super();
        this.myContext = mContext;
        MyApp User = ((MyApp) myContext);
        username = User.getUser();
        ipAddress = User.getIpAddress();
    }

    public String UpdatePlan(final int planID, final String planname, final String begin_date, final String end_date, final int frequency, final String note, final Set<Integer> myfamilies){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("planID", planID);
            params.put("email", username);
            params.put("planname", planname);
            params.put("end_date", end_date);
            params.put("frequency", frequency);
            params.put("note", note);
            params.put("visiblefamilies", myfamilies);
            params.setUseJsonStreamer(true);
            client.post("http://"+ipAddress+":8080/editPlan", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="fail!";
                    Toast.makeText(myContext, "Connecting to server failed!" , Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);
                    int flag = myhtml.getInt("flag");
                    String message = myhtml.getString("message");

                    if(flag == 1){
                        String dbname = username +".db";
                        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                        db = helper.getWritableDatabase();
                        db.beginTransaction();  //开始事务
                        try {
                            ContentValues newplan = new ContentValues();
                            newplan.put("planname", planname);
                            newplan.put("frequency", frequency);
                            newplan.put("note", note);
                            newplan.put("begin_date", begin_date);
                            newplan.put("end_date", end_date);
                            db.update("plan", newplan, "planID = ?", new String[]{planID+""});

                            db.delete("plan_families", "planID = ?", new String[]{planID+""});
				        	/*for(Iterator<Integer> iterator = myfamilies.iterator(); iterator.hasNext();){
				        		ContentValues newvisiblefamilies = new ContentValues();
				        		newvisiblefamilies.put("planID", planID);
				        		newvisiblefamilies.put("familyID", iterator.next());
				        		db.replace("plan_families", null, null);
				        	}*/
                            Iterator<Integer> itr = myfamilies.iterator();
                            while(itr.hasNext()){
                                Integer i = itr.next();
                                ContentValues plan_families = new ContentValues();
                                plan_families.put("planID", planID);
                                plan_families.put("familyID", i);
                                db.insert("plan_families", null, plan_families);
                            }
                            db.setTransactionSuccessful();  //设置事务成功完成
                        } finally {
                            db.endTransaction();    //结束事务
                        }
                        db.close();

                        Toast.makeText(myContext, "Edit plan succeeded!" , Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent();
                        intent.setClass(myContext, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        myContext.startActivity(intent);
                    }
                    else{
                        Toast.makeText(myContext, message , Toast.LENGTH_SHORT).show();
                    }


                    msg="success!";
                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }
}
