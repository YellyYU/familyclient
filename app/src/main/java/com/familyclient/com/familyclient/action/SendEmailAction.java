package com.familyclient.com.familyclient.action;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class SendEmailAction {
    static String msg = "";
    static RequestParams params = new RequestParams();
    static Context myContext;

    public SendEmailAction() {
    }

    public SendEmailAction(Context mContext) {
        super();
        this.myContext = mContext;
    }

    public String SendEmail(String email){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("email", email);
            params.setUseJsonStreamer(true);
            client.post("http://192.168.43.222:8080/configure", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="Connectting to server failed!";
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);

                    int flag = myhtml.getInt("flag");
                    if(flag == 1){
                        Toast.makeText(myContext, "验证码发送成功，请前往邮箱获取", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(myContext, "邮箱错误!请换一个邮箱", Toast.LENGTH_SHORT).show();
                        msg = "fail!";
                    }
                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }
}
