package com.familyclient.com.familyclient.fragment;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.familyclient.R;
import com.familyclient.com.familyclient.action.AttackHandledAction;
import com.familyclient.com.familyclient.action.LogRefreshAction;
import com.familyclient.com.familyclient.action.PlanStatusAction;
import com.familyclient.com.familyclient.activity.LoginActivity;
import com.familyclient.com.familyclient.activity.ModifyPasswordActivity;
import com.familyclient.com.familyclient.activity.ShowPlanActivity;
import com.familyclient.com.familyclient.local.LogViewHolder;
import com.familyclient.com.familyclient.local.MainPageViewHolder;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;
import com.familyclient.com.familyclient.local.UserMainViewHolder;

//public class MyFragment extends Fragment implements OnClickListener

public class UserMainFragment extends Fragment {

    private ListView myList, myList2;
    private static Context myContext;
    private static String username;
    private Button modifypwd, quit;
    private List<String> log_contents = new ArrayList<String>();
    private List<String> log_times = new ArrayList<String>();
    private List<String> log_statuses = new ArrayList<String>();
    private List<String> log_types = new ArrayList<String>();

    private List<String> attacks = new ArrayList<String>();
    private List<Integer> attackIDs = new ArrayList<Integer>();
    private List<String> senders = new ArrayList<String>();
    private static SQLHelper helper;
    private static SQLiteDatabase db;
    private TextView txtZQD1;
    private TextView txtZQD2;

    public UserMainFragment() {
        // Required empty public constructor
    }

    public static UserMainFragment newInstance(Context mContext) {

        myContext = mContext;
        MyApp User = ((MyApp) myContext);
        username = User.getUser();
        String dbname = username +".db";
        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
        db = helper.getWritableDatabase();

        UserMainFragment fragment = new UserMainFragment();
        Bundle args = new Bundle();
        args.putString(username, username);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onHiddenChanged(boolean hidden){
        super.onHiddenChanged(hidden);
        myList.clearAnimation();
        myList2.clearAnimation();
        log_contents.clear();
        log_statuses.clear();
        log_times.clear();
        log_types.clear();
        attacks.clear();
        senders.clear();
        attackIDs.clear();
        LogRefreshAction logfresh = new LogRefreshAction(myContext);
        logfresh.Refresh();

        getLogs();
        LogAdapter logAdapter = new LogAdapter(myContext);
        myList.setAdapter(logAdapter);

        getAttacks();
        final MainAdapter madapter = new MainAdapter(myContext);
        myList2.setAdapter(madapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_main,container,false);
        myList = (ListView) view.findViewById(R.id.user_main_lists);
        myList2=(ListView) view.findViewById(R.id.user_main_list_attacks);
        txtZQD1 = (TextView) view.findViewById(R.id.user_main_username);
        txtZQD1.setText(username);
        txtZQD2 = (TextView) view.findViewById(R.id.user_main_userid);
        String usernameTem = getUsername(username, 0);
        txtZQD2.setText(usernameTem);

        modifypwd = (Button) view.findViewById(R.id.user_main_change_password);
        quit=(Button) view.findViewById(R.id.user_main_quit);

        /*AttackRefreshAction refresh = new AttackRefreshAction(myContext);
        refresh.refresh();*/
        LogRefreshAction logfresh = new LogRefreshAction(myContext);
        logfresh.Refresh();

        //String filename = username + ".credit&punish.txt";
        getLogs();
        LogAdapter logAdapter = new LogAdapter(myContext);
        myList.setAdapter(logAdapter);

        getAttacks();
        final MainAdapter madapter = new MainAdapter(myContext);
        myList2.setAdapter(madapter);

        modifypwd.setOnClickListener(new View.OnClickListener(){
            public void  onClick(View v){
				/*点击跳转到修改密码页面*/
                Intent intent = new Intent();
                intent.setClass(getActivity(), ModifyPasswordActivity.class);
                startActivity(intent);
            }
        });

	    /**/
        quit.setOnClickListener(new View.OnClickListener(){
            public void  onClick(View v){
				/*点击时注销当前账户，然后回到登录页面*/
                Intent intent = new Intent();
                intent.setClass(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        return view;
    }

    private void getAttacks(){
        db.beginTransaction();  //开始事务
        try {
            Cursor cursor = db.query("attack", null, "status = ?", new String[]{"1"}, null, null, null);
            if(cursor.moveToFirst()){
                do{
                    String senderTem = cursor.getString(cursor.getColumnIndex("sender"));
                    String receiverTem = cursor.getString(cursor.getColumnIndex("receiver"));
                    int familyID = cursor.getInt(cursor.getColumnIndex("familyID"));
                    String sender = getUsername(senderTem, familyID);
                    String receiver = getUsername(receiverTem, familyID);
                    String attack_time = cursor.getString(cursor.getColumnIndex("attack_time"));
                    String attack_title = cursor.getString(cursor.getColumnIndex("title"));
                    senders.add(sender);
                    attacks.add(sender+" attacked "+receiver+" "+attack_time+" to "+attack_title);
                    attackIDs.add(cursor.getInt(cursor.getColumnIndex("attackID")));
                }while(cursor.moveToNext());
            }
            db.setTransactionSuccessful();  //设置事务成功完成
            //Toast.makeText(myContext, attacks.size()+"", Toast.LENGTH_SHORT).show();

        } finally {
            db.endTransaction();    //结束事务
        }
        db.close();
    }

    private String getUsername(String useremail, int familyID){
        String result = "";
        db.beginTransaction();  //开始事务
        if(familyID == 0){
            try {
                Cursor cursor = db.query("User", null, "email = ?", new String[]{useremail}, null, null, null);
                if(cursor.moveToFirst()){
                    result = cursor.getString(cursor.getColumnIndex("username"));
                }
                db.setTransactionSuccessful();  //设置事务成功完成
            } finally {
                db.endTransaction();    //结束事务
            }
        }
        else{
            try {
                Cursor cursor = db.query("members"+familyID, null, "email = ?", new String[]{useremail}, null, null, null);
                if(cursor.moveToFirst()){
                    result = cursor.getString(cursor.getColumnIndex("username"));
                }
                db.setTransactionSuccessful();  //设置事务成功完成
            } finally {
                db.endTransaction();    //结束事务
            }
        }

        return result;
    }

    private void getLogs(){
        db = helper.getWritableDatabase();
        db.beginTransaction();
        try {
            Cursor cursor = db.query("log", null, null, null, null, null, "logtime");
            if(cursor.moveToFirst()){
                do{
                    String sender = cursor.getString(cursor.getColumnIndex("sender"));
                    String receiver = cursor.getString(cursor.getColumnIndex("receiver"));
                    String logtime = cursor.getString(cursor.getColumnIndex("logtime"));
                    String type = cursor.getString(cursor.getColumnIndex("type"));
                    log_contents.add(sender+" "+type+" "+receiver);
                    log_statuses.add("");
                    log_types.add(type);
                    log_times.add(logtime);
                }while(cursor.moveToNext());
            }

            Cursor cursor1 = db.query("attack", null, "status = ?", new String[]{"2"}, null, null, "attack_time");
            if(cursor1.moveToFirst()){
                do{
                    String sender = cursor1.getString(cursor1.getColumnIndex("sender"));
                    String receiver = cursor1.getString(cursor1.getColumnIndex("receiver"));
                    String attack_time = cursor1.getString(cursor1.getColumnIndex("attack_time"));
                    String title = cursor1.getString(cursor1.getColumnIndex("title"));
                    log_contents.add(sender+" attacked "+receiver+" to "+title);
                    log_statuses.add("finished");
                    log_types.add("attack");
                    log_times.add(attack_time);
                }while(cursor1.moveToNext());
            }

            Cursor cursor2 = db.query("attack", null, "status = ?", new String[]{"4"}, null, null, null);
            if(cursor2.moveToFirst()){
                do{
                    String sender = cursor2.getString(cursor2.getColumnIndex("sender"));
                    String receiver = cursor2.getString(cursor2.getColumnIndex("receiver"));
                    String attack_time = cursor2.getString(cursor2.getColumnIndex("attack_time"));
                    String title = cursor2.getString(cursor2.getColumnIndex("title"));
                    log_contents.add(sender+" attacked "+receiver+" to "+title);
                    log_statuses.add("resisted");
                    log_types.add("attack");
                    log_times.add(attack_time);
                }while(cursor2.moveToNext());
            }

            Cursor cursor3 = db.query("attack", null, "status = ?", new String[]{"0"}, null, null, null);
            if(cursor3.moveToFirst()){
                do{
                    String sender = cursor3.getString(cursor3.getColumnIndex("sender"));
                    String receiver = cursor3.getString(cursor3.getColumnIndex("receiver"));
                    String attack_time = cursor3.getString(cursor3.getColumnIndex("attack_time"));
                    String title = cursor3.getString(cursor3.getColumnIndex("title"));
                    log_contents.add(sender+" attacked "+receiver+" to "+title);
                    log_statuses.add("sent");
                    log_types.add("attack");
                    log_times.add(attack_time);
                }while(cursor3.moveToNext());
            }

            db.setTransactionSuccessful();  //设置事务成功完成
        } finally {
            db.endTransaction();    //结束事务
        }
		    /*FileInputStream input;
			input = myContext.openFileInput(filename);
			//用bufferedReader按行读取
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input));

	        String str = null;
	        while((str = bufferedReader.readLine()) != null)
	        {
	        	result.add(str);
	        }
			Log.i("size", result.size()+"");*/
    }


    public class LogAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public LogAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return log_contents.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        private int findTypeResource(String type){
            if(type.equals("attack")){
                return R.drawable.boom;
            }//TODO: I don't know whether the representation is right!!!
            else if(type.equals("credit")){
                return R.drawable.flower;
            }
            else if(type.equals("punish")){
                return R.drawable.poop;
            }
            else{
                System.err.println("undefined log type: " + type);
                return R.drawable.question;
            }
        }

        @SuppressLint("InflateParams")
        public View getView(final int position, View convertView, ViewGroup parent) {

            LogViewHolder holder=new LogViewHolder();
            convertView = mInflater.inflate(R.layout.main_page_list_item, null);

            holder.content=(TextView) convertView.findViewById(R.id.user_main_log_list_content);
            holder.content.setText(log_contents.get(position));
            //holder.complete.setChecked(dos[position]);

            holder.type=(TextView)convertView.findViewById(R.id.user_main_log_list_icon);
            holder.type.setBackgroundResource(findTypeResource(log_types.get(position)));

            holder.statues = (TextView)convertView.findViewById(R.id.user_main_log_list_status);
            holder.statues.setText(log_statuses.get(position));

            holder.time = (TextView)convertView.findViewById(R.id.user_main_log_list_item_time);
            holder.time.setText(log_times.get(position));
            return convertView;
        }
    }

    public class MainAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public MainAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return attacks.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @SuppressLint("InflateParams")
        public View getView(final int position, View convertView, ViewGroup parent) {

            UserMainViewHolder holder=new UserMainViewHolder();
            convertView = mInflater.inflate(R.layout.user_main_list_item2, null);
            holder.attack=(TextView)convertView.findViewById(R.id.user_main_list_item2_attack);
            holder.defence=(Button)convertView.findViewById(R.id.user_main_list_item2_defence);
            holder.finish=(Button)convertView.findViewById(R.id.user_main_list_item2_finish);
            holder.attack.setText(attacks.get(position));

            if(username.equals(senders.get(position))){
                holder.defence.setClickable(false);
                holder.finish.setClickable(false);
            }
            holder.defence.setOnClickListener(new View.OnClickListener(){
                public void  onClick(View v){
                    //记得传参数？
                    int attackID = attackIDs.get(position);
                    AttackHandledAction post = new AttackHandledAction(myContext);
                    post.attackResist(attackID);
                    Toast.makeText(myContext, "You have resisted the attack:)", Toast.LENGTH_SHORT).show();
                }
            });

            holder.finish.setOnClickListener(new View.OnClickListener(){
                public void  onClick(View v){
                    //记得传参数？
                    int attackID = attackIDs.get(position);
                    AttackHandledAction post = new AttackHandledAction(myContext);
                    post.attackDone(attackID);
                    Toast.makeText(myContext, "You have finished the attack:)", Toast.LENGTH_SHORT).show();
                }
            });
            return convertView;
        }
    }
}


