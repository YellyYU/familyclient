package com.familyclient.com.familyclient.activity;

import java.util.ArrayList;
import java.util.List;

import com.familyclient.R;
import com.familyclient.com.familyclient.local.MemberPlansViewHolder;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.RoundProgressBarWithNumber;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class MemberPlansActivity extends Activity {

    private TextView txtZQD;
    static RequestParams params = new RequestParams();
    //public static String plan_name[]=new String[]{"计划0","计划1","计划2","计划3","计划4","计划5","计划6","计划7","计划8","计划9"};
    //public static Integer progress[]=new Integer[]{ 9,8,7,6,5,4,5,6,7,8};
    //public static Integer total[]=new Integer[]{ 10,9,8,7,6,5,6,7,8,9};
    private List<String> plans = new ArrayList<String>();
    private List<Integer> progress = new ArrayList<Integer>();
    private List<Integer> total = new ArrayList<Integer>();

    private String memberEmail = "";
    private int familyID;
    private int member_credit_num, member_punish_num, member_progress_val, member_progress_max;

    private Context mContext;

    private String username;
    private String ipAddress;

    private RoundProgressBarWithNumber mRoundProgressBar;
    private TextView txt_credit_num, txt_punish_num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
        setContentView(R.layout.activity_member_plans);
        //新页面接收数据
        Bundle bundle = this.getIntent().getExtras();
        //接收planID值
        memberEmail = bundle.getString("memberEmail");
        familyID = bundle.getInt("familyID");
        member_credit_num = bundle.getInt("creditNumber");
        member_punish_num = bundle.getInt("punishNumber");
        member_progress_val = bundle.getInt("progress_val");
        member_progress_max = bundle.getInt("progress_max");

        MyApp User = ((MyApp) mContext);
        username = User.getUser();
        ipAddress = User.getIpAddress();

        mRoundProgressBar = (RoundProgressBarWithNumber) findViewById(R.id.id_progress02);
        mRoundProgressBar.setProgress(member_progress_val);
        mRoundProgressBar.setMax(member_progress_max);

        txt_credit_num = (TextView)findViewById(R.id.members_zan_number);
        txt_credit_num.setText(""+member_credit_num);
        txt_punish_num = (TextView)findViewById(R.id.members_cai_number);
        txt_punish_num.setText(""+member_punish_num);

        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("email", memberEmail);
            params.put("familyID", familyID);
            params.setUseJsonStreamer(true);
            client.post("http://"+ipAddress+":8080/memberPlanPage", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    Toast.makeText(mContext, "Connecting to server failed!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);

                    int flag = myhtml.getInt("flag");
                    String message = myhtml.getString("message");
                    if(flag == 1){
                        List<JSONObject> memberPlans = myhtml.getJSONArray("memberPlans");
                        for(int i=0; i<memberPlans.size();i++){
                            JSONObject thePlan = memberPlans.get(i);
                            plans.add(thePlan.getString("planname"));
                            progress.add(thePlan.getInt("total_done"));
                            // TODO total should be named
                            total.add(10);
                        }
                        //Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                        ListView list=(ListView)findViewById(R.id.family_member_plans_list);
                        FamilyMemberPlansAdapter plan_adapter = new FamilyMemberPlansAdapter(mContext);
                        list.setAdapter(plan_adapter);
                    }
                    else{
                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch(Exception e){e.printStackTrace();}

		/*设置头像*/
        txtZQD = (TextView) findViewById(R.id.family_member_plans_username);
        Drawable[] drawable = txtZQD.getCompoundDrawables();
        // 数组下表0~3,依次是:左上右下
        drawable[0].setBounds(0, 0, 120, 120);
        txtZQD.setCompoundDrawables(drawable[0], null, null, null);
        txtZQD.setText(memberEmail);
    }


    public class FamilyMemberPlansAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public FamilyMemberPlansAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return plans.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }


        @SuppressLint({ "ViewHolder" })
        public View getView(final int position, View convertView, ViewGroup parent) {

            MemberPlansViewHolder holder=new MemberPlansViewHolder();
            convertView = mInflater.inflate(R.layout.family_member_plans_list_item, null);
            holder.plan_name=(TextView)convertView.findViewById(R.id.family_member_plans_list_item_name);
            holder.progress=(ProgressBar)convertView.findViewById(R.id.family_member_plans_list_item_progress);

            holder.plan_name.setText(plans.get(position));
            holder.progress.setMax(total.get(position));
            holder.progress.setProgress(progress.get(position));

            return convertView;
        }


    }
}
