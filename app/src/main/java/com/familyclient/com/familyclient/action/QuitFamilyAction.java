package com.familyclient.com.familyclient.action;

import com.familyclient.com.familyclient.activity.MainActivity;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class QuitFamilyAction {
    static String msg = "";
    static RequestParams params = new RequestParams();
    static Context myContext;
    private String username;
    private SQLHelper helper;
    private SQLiteDatabase db;
    private String ipAddress;

    public QuitFamilyAction() {
    }

    public QuitFamilyAction(Context mContext) {
        super();
        this.myContext = mContext;
        MyApp User = ((MyApp) myContext);
        username = User.getUser();
        ipAddress = User.getIpAddress();
    }

    public String QuitFamily(final int familyID){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("email", username);
            params.put("familyID", familyID);
            params.setUseJsonStreamer(true);
            client.post("http://"+ipAddress+":8080/exitFamily", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="fail!";
                    Toast.makeText(myContext, "Connecting to server failed!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);

                    int flag = myhtml.getInt("flag");
                    String message = myhtml.getString("message");
                    if(flag == 1){
                        String dbname = username+".db";
                        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                        db = helper.getWritableDatabase();
                        db.beginTransaction();
                        db.delete("familygroup", "familyID = ?", new String[]{familyID+""});
                        db.delete("plan_families", "familyID = ?", new String[]{familyID+""});
                        db.execSQL("DROP TABLE IF EXISTS members"+familyID);
                        db.setTransactionSuccessful();
                        db.endTransaction();
                        db.close();
                        Toast.makeText(myContext, "Quit family succeeded!", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent();
                        intent.setClass(myContext, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        myContext.startActivity(intent);
                    }
                    else{
                        Toast.makeText(myContext, message, Toast.LENGTH_SHORT).show();
                    }
                    msg="success!";

                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }
}
