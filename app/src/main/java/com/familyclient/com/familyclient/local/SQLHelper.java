package com.familyclient.com.familyclient.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLHelper extends SQLiteOpenHelper{

    public static int currVer = 2;

    public SQLHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, null, version);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
		/*db.execSQL("DROP DATABASE IF EXISTS family");
		db.execSQL("CREATE DATABASE family");*/
        db.execSQL("DROP TABLE IF EXISTS User");
        db.execSQL("DROP TABLE IF EXISTS Plan");
        db.execSQL("DROP TABLE IF EXISTS familygroup");
        db.execSQL("DROP TABLE IF EXISTS log");
        db.execSQL("DROP TABLE IF EXISTS attack");
        db.execSQL("DROP TABLE IF EXISTS plan_families");
        db.execSQL("CREATE TABLE User(email VARCHAR(50) PRIMARY KEY,username VARCHAR(1024),password VARCHAR(1024))");
        db.execSQL("CREATE TABLE Plan(planID INTEGER PRIMARY KEY, planname VARCHAR(40), begin_date VARCHAR(40), end_date VARCHAR(40), frequency INTEGER, note varchar(50), today_done INTEGER, total_done INTEGER)");
        db.execSQL("CREATE TABLE familygroup(familyID INTEGER PRIMARY KEY, family_name VARCHAR(50))");
        db.execSQL("CREATE TABLE log(logID INTEGER PRIMARY KEY, logtime TIMESTAMP, sender VARCHAR(512) NOT NULL, receiver VARCHAR(512) NOT NULL, type VARCHAR(512))");
        db.execSQL("CREATE TABLE attack(attackID INTEGER PRIMARY KEY, familyID INTEGER, sender VARCHAR(50), receiver VARCHAR(50), attack_time TIMESTAMP, status INTEGER, title VARCHAR(50), note VARCHAR(512))");
        db.execSQL("CREATE TABLE plan_families(id INTEGER PRIMARY KEY AUTOINCREMENT, planID INTEGER, familyID INTEGER)");
		/*db.delete("User", null, null);
		db.delete("Plan", null, null);
		db.delete("familygroup", null, null);
		db.delete("log", null, null);
		db.delete("attack", null, null);
		db.delete("plan_families", null, null);*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS User");
        db.execSQL("DROP TABLE IF EXISTS Plan");
        db.execSQL("DROP TABLE IF EXISTS familygroup");
        db.execSQL("DROP TABLE IF EXISTS log");
        db.execSQL("DROP TABLE IF EXISTS attack");
        db.execSQL("DROP TABLE IF EXISTS plan_families");
        db.execSQL("CREATE TABLE User(email VARCHAR(50) PRIMARY KEY,username VARCHAR(1024),password VARCHAR(1024))");
        db.execSQL("CREATE TABLE Plan(planID INTEGER PRIMARY KEY, planname VARCHAR(40), begin_date VARCHAR(40), end_date VARCHAR(40), frequency INTEGER, note varchar(50), today_done INTEGER, total_done INTEGER)");
        db.execSQL("CREATE TABLE familygroup(familyID INTEGER PRIMARY KEY, family_name VARCHAR(50))");
        db.execSQL("CREATE TABLE log(logID INTEGER PRIMARY KEY, logtime TIMESTAMP, sender VARCHAR(512) NOT NULL, receiver VARCHAR(512) NOT NULL, type VARCHAR(512))");
        db.execSQL("CREATE TABLE attack(attackID INTEGER PRIMARY KEY, familyID INTEGER, sender VARCHAR(50), receiver VARCHAR(50), attack_time TIMESTAMP, status INTEGER, title VARCHAR(50), note VARCHAR(512))");
        db.execSQL("CREATE TABLE plan_families(id INTEGER PRIMARY KEY AUTOINCREMENT, planID INTEGER, familyID INTEGER)");

    }

}
