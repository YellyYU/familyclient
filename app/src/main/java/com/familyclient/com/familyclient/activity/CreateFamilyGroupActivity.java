package com.familyclient.com.familyclient.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.familyclient.R;
import com.familyclient.com.familyclient.action.CreateFamilyAction;
import com.familyclient.com.familyclient.local.MyApp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CreateFamilyGroupActivity extends Activity {
    private EditText nickname, password;
    private Context mContext;
    private int familyAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_family_group);
        mContext = getApplicationContext();
        MyApp User = ((MyApp) mContext);
        familyAmount = User.getFamilyAmount();
        nickname = (EditText) findViewById(R.id.create_family_group_nickname);
        password = (EditText) findViewById(R.id.create_family_group_password);


	    /*设置创建与取消按钮*/
        Button create=(Button)findViewById(R.id.create_family_group_create);
        create.setOnClickListener(new View.OnClickListener(){
            public void  onClick(View v){
                String nicknameValue = nickname.getText().toString();
                String pwdValue = password.getText().toString();
                if(nicknameValue.equals("")||pwdValue.equals("")||pwdValue.equals("")){
                    Toast.makeText(CreateFamilyGroupActivity.this, "输入不得为空!", Toast.LENGTH_SHORT).show();
                }
                else if(pwdValue.length()<8||pwdValue.length()>28){
                    Toast.makeText(CreateFamilyGroupActivity.this, "密码长度应在8—28之间!", Toast.LENGTH_SHORT).show();
                }
                else if(nicknameValue.length()>30){
                    Toast.makeText(CreateFamilyGroupActivity.this, "昵称长度应少于15个字!", Toast.LENGTH_SHORT).show();
                }
                else if(familyAmount>4){
                    Toast.makeText(CreateFamilyGroupActivity.this, "您的家庭组数量已达到5个，无法再添加!", Toast.LENGTH_SHORT).show();
                }
                else{
                    CreateFamilyAction post = new CreateFamilyAction(mContext);
                    String result = post.CreateFamily(nickname.getText().toString(), password.getText().toString());
                    Toast.makeText(CreateFamilyGroupActivity.this, result, Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button cancel=(Button)findViewById(R.id.create_family_group_cancel);
        cancel.setOnClickListener(new View.OnClickListener(){
            public void  onClick(View v){
				/*取消按钮，返回家庭组页面？还有其他操作可以继续加，*/
                Intent intent = new Intent();
                intent.setClass(CreateFamilyGroupActivity.this, FamilyGroupActivity.class);
                startActivity(intent);
            }
        });

    }
}
