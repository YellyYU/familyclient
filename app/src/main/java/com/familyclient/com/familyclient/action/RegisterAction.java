package com.familyclient.com.familyclient.action;

import com.familyclient.com.familyclient.activity.LoginActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class RegisterAction {
    static String msg = "";
    static RequestParams params = new RequestParams();
    static Context myContext;

    public RegisterAction() {
    }

    public RegisterAction(Context mContext) {
        super();
        this.myContext = mContext;
    }

    public String Register(final String email, final String password, final String username, final String auth){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("email", email);
            params.put("name", username);
            params.put("pwd", password);
            params.put("codes", auth);
            params.setUseJsonStreamer(true);
            client.post("http://192.168.1.104:8080/register", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="连接服务器失败!";
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);

                    int flag = myhtml.getInt("flag");
                    String message = myhtml.getString("message");
                    if(flag == 1){
                        Toast.makeText(myContext, "Register succeeded!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent();
                        intent.setClass(myContext, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        myContext.startActivity(intent);
                    }else{
                        Toast.makeText(myContext, message, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }
}
