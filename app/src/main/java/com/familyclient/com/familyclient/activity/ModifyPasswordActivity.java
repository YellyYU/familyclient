package com.familyclient.com.familyclient.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.familyclient.R;
import com.familyclient.com.familyclient.action.ModifyPwdAction;
import com.familyclient.com.familyclient.local.Encoding;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;

public class ModifyPasswordActivity extends Activity {
	
	private EditText oldPwd, newPwd, confirmPwd;
	private Button cancel, handin;
    private Context myContext;
    String username;
    private SQLHelper helper;
	private SQLiteDatabase db;
	     
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_password);
		myContext = getApplicationContext();
		MyApp User = ((MyApp) myContext);
	    username = User.getUser();
		String dbname = username+".db";
		helper = new SQLHelper(myContext, dbname, null, 1);
		db = helper.getWritableDatabase(); 
		
		setTitle("Modify Password");
		oldPwd = (EditText) findViewById(R.id.change_password_old);
		newPwd = (EditText) findViewById(R.id.change_password_new);
		confirmPwd = (EditText) findViewById(R.id.change_password_confirm_new);
        cancel = (Button) findViewById(R.id.change_password_cancel);
        handin = (Button) findViewById(R.id.change_password_handin);
        
        
        cancel.setOnClickListener(new Button.OnClickListener(){
        	@Override
             public void onClick(View v) {
        		Intent intent = new Intent();
    			intent.setClass(ModifyPasswordActivity.this, MainActivity.class);
    			startActivity(intent);
        	 }
        });
		
        handin.setOnClickListener(new Button.OnClickListener() {

        @Override
        public void onClick(View v) {
        	String oldpwd = oldPwd.getText().toString();
        	String newpwd = newPwd.getText().toString();
        	String confirm_new = confirmPwd.getText().toString();
        	/*Firstly check whether the old_pwd is correct*/
			db.beginTransaction();
        	Cursor cursor = db.query("User", null, null, null, null, null, null);
        	String password = "";
        	if (cursor.moveToFirst()) {
                do {
                    password = cursor.getString(cursor.getColumnIndex("password"));
                } while (cursor.moveToNext());
            }
        	db.endTransaction();
        	
        	//�Ƚ��������
        	Encoding encoder = new Encoding();
        	String pwdEncoded = encoder.getMD5x99(oldpwd);
        	/*check whether the old_pwd is correct*/
			if(!pwdEncoded.equals(password)){//if the pwd is wrong
				Toast.makeText(myContext, "�������������!", Toast.LENGTH_SHORT).show();
			}else{
				String new_pwd = encoder.getMD5x99(newpwd);
				ModifyPwdAction post = new ModifyPwdAction(myContext);
				post.Modify(new_pwd);
			}
        }
    });
}
	
};
