package com.familyclient.com.familyclient.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.familyclient.R;
import com.familyclient.com.familyclient.action.LoginAction;
import com.familyclient.com.familyclient.local.Encoding;
import com.familyclient.com.familyclient.local.ToastHelper;

public class LoginActivity extends Activity {

    private EditText email, pwd;
    private CheckBox rem_pw, auto_login;
    private Button btn_login, btn_register;
    private Context myContext;

    private ToastHelper toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        myContext = getApplicationContext();
        toast = new ToastHelper();

        setTitle("login");
        email = (EditText) findViewById(R.id.login_account);
        pwd = (EditText) findViewById(R.id.login_password);
        rem_pw = (CheckBox) findViewById(R.id.login_rememberPassword);
        auto_login = (CheckBox) findViewById(R.id.login_autologin);
        btn_login = (Button) findViewById(R.id.login_login);
        btn_register = (Button) findViewById(R.id.login_register);

        btn_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String emailValue = email.getText().toString();
                String pwdValue = pwd.getText().toString();
                if (emailValue.equals("") || pwdValue.equals("")){
                    //Toast.makeText(LoginActivity.this, "用户名/密码不得为空", Toast.LENGTH_SHORT).show();
                    toast.showToast(myContext, "用户名/密码不得为空");
                }
                else if(pwdValue.length()<8||pwdValue.length()>28){
                    //Toast.makeText(LoginActivity.this, "密码长度必须为8~28!", Toast.LENGTH_SHORT).show();
                    toast.showToast(myContext, "密码长度必须为8~28!");
                }
                else {
                    //先将密码加密
                    Encoding encoder = new Encoding();
                    String pwdEncoded = encoder.getMD5x99(pwdValue);
                    String pwdtest = encoder.getMD5x99("jiahuihiehaha");
                    Log.i("encoded", pwdEncoded);
                    Log.i("encodedTest", pwdtest);
            	/*传送到服务器，验证数据库*/
            	/*
            	 * 先
            	 * 不
            	 * 传
            	 * 送
            	 * 服
            	 * 务
            	 * 器
            	 * ，
            	 * 进
            	 * 行
            	 * 直
            	 * 接
            	 * 登
            	 * 录
            	 *
            	 *
            	 */
                    LoginAction login = new LoginAction(myContext);
                    String msg = "";
                    msg = login.Auth(emailValue, pwdEncoded);
            	/*MyApp User = ((MyApp) myContext);
				User.setUser(emailValue);
				User.setFamilyAmount(2);
            	Intent intent = new Intent();
    			intent.setClass(myContext, MainActivity.class);
    			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    			myContext.startActivity(intent);*/
                }
            }
        });

        btn_register.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

};
