package com.familyclient.com.familyclient.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.familyclient.R;
import com.familyclient.com.familyclient.action.RegisterAction;
import com.familyclient.com.familyclient.action.SendEmailAction;
import com.familyclient.com.familyclient.local.Encoding;

public class RegisterActivity extends Activity {

    private EditText email, pwd, username, repeat_pwd, auth;
    private Button register, email_auth;
    private Context myContext;
    private String msg = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        myContext = getApplicationContext();

        setTitle("Register");
        email = (EditText) findViewById(R.id.register_account);
        pwd = (EditText) findViewById(R.id.register_password);
        username = (EditText) findViewById(R.id.register_username);
        repeat_pwd = (EditText) findViewById(R.id.register_confirmPassword);
        register = (Button) findViewById(R.id.register_register);
        email_auth = (Button) findViewById(R.id.register_get_test);
        auth = (EditText) findViewById(R.id.register_write_test);


        email_auth.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Toast.makeText(RegisterActivity.this, "不得重复发送!", Toast.LENGTH_SHORT).show();
                SendEmailAction post = new SendEmailAction(myContext);
                post.SendEmail(email.getText().toString());
            }
        });

        register.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String emailValue = email.getText().toString();
                String usernameValue = username.getText().toString();
                String pwdValue = pwd.getText().toString();
                String authValue = auth.getText().toString();
                String pwdValue_confirm = repeat_pwd.getText().toString();
                if(emailValue.equals("")||usernameValue.equals("")||pwdValue.equals("")){
                    Toast.makeText(RegisterActivity.this, "输入不得为空!", Toast.LENGTH_SHORT).show();
                }
                else if (!pwdValue.equals(pwdValue_confirm)){
                    Toast.makeText(RegisterActivity.this, "密码两次输入不同!", Toast.LENGTH_SHORT).show();
                }
                else if(pwdValue.length()<8||pwdValue.length()>28){
                    Toast.makeText(RegisterActivity.this, "密码长度必须为8~28!", Toast.LENGTH_SHORT).show();
                }
                else if(usernameValue.length()>30){
                    Toast.makeText(RegisterActivity.this, "昵称长度不得大于15字!", Toast.LENGTH_SHORT).show();
                }
                else {
                    //先将密码加密
                    Encoding encoder = new Encoding();
                    String pwdEncoded = encoder.getMD5x99(pwdValue);

                    RegisterAction register = new RegisterAction(myContext);
                    msg = register.Register(emailValue, pwdEncoded, usernameValue, authValue);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

};
