package com.familyclient.com.familyclient.action;

import java.util.List;

import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class FamilyRefreshAction {
    static String msg = "";
    static RequestParams params = new RequestParams();
    static Context myContext;
    private SQLHelper helper;
    private SQLiteDatabase db;
    private String username;
    private String ipAddress;

    public FamilyRefreshAction() {
    }

    public FamilyRefreshAction(Context mContext) {
        super();
        this.myContext = mContext;
        MyApp User = ((MyApp) myContext);
        username = User.getUser();
        ipAddress = User.getIpAddress();
    }

    public String refresh(final int familyID){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("email", username);
            params.put("familyID", familyID);
            params.setUseJsonStreamer(true);
            client.post("http://"+ipAddress+":8080/familyPage", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="Connecting to server failed!";
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);
                    int flag = myhtml.getInt("flag");

                    if(flag == 1){
                        JSONObject family = myhtml.getJSONObject("family");
                        List<JSONObject> members = family.getJSONArray("members");

                        String dbname = username+".db";
                        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                        db = helper.getWritableDatabase();
                        db.beginTransaction();  //开始事务
                        String tablename = "members"+familyID;

                        db.execSQL("DROP TABLE IF EXISTS "+tablename);
                        db.execSQL("CREATE TABLE "+tablename+"(email VARCHAR(50) PRIMARY KEY,username VARCHAR(1024), total_plan INTEGER, today_task_done INTEGER, total_punish INTEGER, total_credit INTEGER, total_attack INTEGER, available_credit INTEGER, available_punish INTEGER, week_score INTEGER)");

                        for(int i=0;i<members.size();i++){
                            JSONObject theMember = members.get(i);
                            ContentValues member_insert = new ContentValues();
                            member_insert.put("email", theMember.getString("email"));
                            member_insert.put("username", theMember.getString("username"));
                            member_insert.put("total_plan", theMember.getInt("total_plan"));
                            member_insert.put("today_task_done", theMember.getInt("today_task_done"));
                            member_insert.put("total_punish", theMember.getInt("total_punish"));
                            member_insert.put("total_credit", theMember.getInt("total_credit"));
                            member_insert.put("total_attack", theMember.getInt("total_attack"));
                            member_insert.put("available_credit", theMember.getInt("available_credit"));
                            member_insert.put("available_punish", theMember.getInt("available_punish"));
                            member_insert.put("week_score", theMember.getInt("week_score"));
                            db.replace(tablename, null, member_insert);
                        }
                        db.setTransactionSuccessful();
                        db.endTransaction();

                        msg="1";
                    }
                    else if(flag == 2){
                        msg = myhtml.getString("message");
                    }
                    else{
                        msg = myhtml.getString("message");
                    }
                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }
}
