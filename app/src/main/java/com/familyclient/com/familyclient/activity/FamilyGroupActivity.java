package com.familyclient.com.familyclient.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

import com.familyclient.R;
import com.familyclient.com.familyclient.action.FamilyRefreshAction;
import com.familyclient.com.familyclient.action.QuitFamilyAction;
import com.familyclient.com.familyclient.local.FamilyGroupViewHolder;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;

public class FamilyGroupActivity extends Activity {
    private TextView txtZQD;
    private Spinner choice;

    List<Map<String, Object>>mData;
    private List<String> name = new ArrayList<String>();
    private List<String> progress = new ArrayList<String>();
    private SQLHelper helper;
    private SQLiteDatabase db;
    private String username;
    private Context myContext;
    private int familyID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myContext = getApplicationContext();
        MyApp User = ((MyApp) myContext);
        username = User.getUser();
        String dbname = username+".db";
        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
        db = helper.getWritableDatabase();

        setContentView(R.layout.activity_family_group);
        final ListView listView = (ListView) findViewById(R.id.family_group_members_list);

        /*设置头像*/
        txtZQD = (TextView) findViewById(R.id.family_group_username);
        Drawable[] drawable = txtZQD.getCompoundDrawables();
        // 数组下表0~3,依次是:左上右下
        drawable[0].setBounds(0, 0, 120, 120);
        txtZQD.setCompoundDrawables(drawable[0], null, null, null);


	    /*点击button出现菜单*/
        final Button button = (Button) findViewById(R.id.family_group_operation);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenu(button);
            }
        });

        /*初始化下拉列表*/
        choice=(Spinner) findViewById(R.id.family_group_choice);
        initChoice();

        choice.setOnItemSelectedListener(new OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent,View view,int position,long id){
                Spinner spinner=(Spinner)parent;
                String select=(String)spinner.getItemAtPosition(position);
                String familyid = "";

                for(int i=0;i<select.length();i++){
                    if(select.charAt(i)==' '){
                        break;
                    }
                    familyid+=select.charAt(i);
                }
                Log.i("familyid", familyid);
                familyID = Integer.parseInt(familyid);

        		/*拿到familyID后，发送family查看是否需要更新*/
                FamilyRefreshAction familyRefresh = new FamilyRefreshAction(myContext);
                familyRefresh.refresh(familyID);

        		/*更新之后，从本地读取该familyID的members*/

                db.beginTransaction();
                Cursor cursor = db.query("members"+familyID, null, null, null, null, null, null);
                if (cursor.moveToFirst()) {
                    do {
                        String username = cursor.getString(cursor.getColumnIndex("user"));
                        String today_task_done = cursor.getString(cursor.getColumnIndex("today_task_done"));
                        String total_plan = cursor.getString(cursor.getColumnIndex("total_plan"));
                        name.add(username);
                        progress.add(today_task_done+"/"+total_plan);
                    } while (cursor.moveToNext());
                }
                db.endTransaction();
                db.close();

                mData = getData(name, progress);
                MyAdapter adapter = new MyAdapter(myContext);
                listView.setAdapter(adapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

    }
    /*点击按钮弹出选项*/
    private void showPopupMenu(View view) {
        // View当前PopupMenu显示的相对View的位置
        PopupMenu popupMenu = new PopupMenu(this, view);
        // menu布局
        popupMenu.getMenuInflater().inflate(R.menu.family_group_operation, popupMenu.getMenu());
        // menu的item点击事件
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getTitle().equals("创建家庭组")){
                    Intent intent = new Intent();
                    intent.setClass(FamilyGroupActivity.this, CreateFamilyGroupActivity.class);
                    startActivity(intent);
                }
                if(item.getTitle().equals("加入家庭组")){
                    Intent intent = new Intent();
                    intent.setClass(FamilyGroupActivity.this, JoinFamilyGroupActivity.class);
                    startActivity(intent);
                }
                if(item.getTitle().equals("退出家庭组")){
                    QuitFamilyAction post = new QuitFamilyAction(myContext);
                    String result = post.QuitFamily(familyID);
                }
                //Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        popupMenu.show();
    }

    /*初始化下拉列表*/
    private void initChoice(){

        List<String> items = getSelectItems();
        //1.上下文 2.列表项的资源ID 3.项资源
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,items);
        choice.setAdapter(adapter);
    }

    /*辅助初始化下拉列表*/
    private List<String> getSelectItems(){
        List<String> result = new ArrayList<String>();
        db.beginTransaction();
        Cursor cursor = db.query("familygroup", null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                int familyID = cursor.getInt(cursor.getColumnIndex("familyID"));
                String family_name = cursor.getString(cursor.getColumnIndex("family_name"));
                result.add(familyID+" "+family_name);
            } while (cursor.moveToNext());
        }
        db.endTransaction();
        db.close();
        return result;
    }

    /*获取动态数组数据  可以由其他地方传来(json等)*/
    private List<Map<String, Object>> getData(List<String> name, List<String> progress) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        for(int i=0;i<name.size();i++){
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("name", name.get(i));
            map.put("progress", progress.get(i));
            list.add(map);
        }
        return list;
    }

    public class MyAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public MyAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            FamilyGroupViewHolder holder = null;

            if (convertView == null) {
                holder=new FamilyGroupViewHolder();

//  					//可以理解为从vlist获取view  之后把view返回给ListView
                convertView = mInflater.inflate(R.layout.family_group_member, null);
                holder.name = (TextView)convertView.findViewById(R.id.members_name);
                holder.cai = (Button)convertView.findViewById(R.id.members_cai);
                holder.zan=(Button)convertView.findViewById(R.id.members_zan);
                convertView.setTag(holder);
            }else {
                holder = (FamilyGroupViewHolder)convertView.getTag();
            }

            CaiListener caiListener=new CaiListener(position);
            ZanListener zanListener=new ZanListener(position);

            holder.name.setText((String)mData.get(position).get("name"));
//            holder.progress.setText((String)mData.get(position).get("progress"));
            holder.cai.setTag(position);
            holder.zan.setTag(position);
            //给Button添加单击事件  添加Button之后ListView将失去焦点  需要的直接把Button的焦点去掉
            holder.cai.setOnClickListener(caiListener);
            holder.zan.setOnClickListener(zanListener);
            return convertView;
        }
    }

    private class CaiListener implements OnClickListener{
        int mPosition;
        public CaiListener(int inPosition){
            mPosition= inPosition;
        }
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            Toast.makeText(FamilyGroupActivity.this, name.get(mPosition), Toast.LENGTH_SHORT).show();
            Button cai=(Button)v;
            cai.setBackgroundResource(R.drawable.poop);
        }

    }

    private class ZanListener implements OnClickListener{
        int mPosition;
        public ZanListener(int inPosition){
            mPosition= inPosition;
        }
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            Toast.makeText(FamilyGroupActivity.this, name.get(mPosition), Toast.LENGTH_SHORT).show();
            Button zan=(Button)v;
            zan.setBackgroundResource(R.drawable.flower);
        }

    }

}
