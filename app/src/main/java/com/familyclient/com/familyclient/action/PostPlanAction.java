package com.familyclient.com.familyclient.action;

import java.util.Iterator;
import java.util.Set;

import com.familyclient.com.familyclient.activity.MainActivity;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class PostPlanAction {
    static String msg = "";
    static RequestParams params = new RequestParams();
    static Context myContext;
    private SQLHelper helper;
    private SQLiteDatabase db;
    private String username;
    private String ipAddress;

    public PostPlanAction() {
    }

    public PostPlanAction(Context mContext) {
        super();
        this.myContext = mContext;
        MyApp User = ((MyApp) myContext);
        username = User.getUser();
        ipAddress = User.getIpAddress();
    }

    public String CreatePlan(final String planname, final String begin_date, final String end_date, final int frequency, final String note, final Set<Integer> myfamilies){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("email", username);
            params.put("planname", planname);
            params.put("begin_date", begin_date);
            params.put("end_date", end_date);
            params.put("frequency", frequency);
            params.put("note", note);
            params.put("visiblefamilies", myfamilies);
            params.setUseJsonStreamer(true);
            client.post("http://"+ipAddress+":8080/createPlan", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="fail!";
                    Toast.makeText(myContext, "Connecting to server failed!" , Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);
                    int flag = myhtml.getInt("flag");
                    String message = myhtml.getString("message");

                    if(flag == 1){
                        int planID = myhtml.getInt("id");
                        String dbname = username+".db";
                        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                        db = helper.getWritableDatabase();
                        db.beginTransaction();  //开始事务
                        try {
                            ContentValues newplan = new ContentValues();
                            newplan.put("planID", planID);
                            //newplan.put("visiblefamilies", myfamilies.toString());
                            newplan.put("planname", planname);
                            newplan.put("frequency", frequency);
                            newplan.put("note", note);
                            newplan.put("begin_date", begin_date);
                            newplan.put("end_date", end_date);
                            newplan.put("today_done", 0);
                            newplan.put("total_done", 0);
                            //db.execSQL("INSERT INTO person(name) values(?)",new String[]{"zhanglei"});
                            db.insert("plan", null, newplan);

				        	/*for(int i : myfamilies){
				        		ContentValues plan_families = new ContentValues();
				        		plan_families.put("planID", i);
				        		db.insert("plan_families", null, plan_families);
				        	}*/
                            Iterator<Integer> itr = myfamilies.iterator();
                            while(itr.hasNext()){
                                Integer i = itr.next();
                                ContentValues plan_families = new ContentValues();
                                plan_families.put("planID", planID);
                                plan_families.put("familyID", i);
                                db.insert("plan_families", null, plan_families);
                            }

                            db.setTransactionSuccessful();  //设置事务成功完成
                        } finally {
                            db.endTransaction();    //结束事务
                        }
                        db.close();
                        Toast.makeText(myContext, "Create plan succeeded!" , Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent();
                        intent.setClass(myContext, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        myContext.startActivity(intent);
                    }
                    else{
                        Toast.makeText(myContext, message , Toast.LENGTH_SHORT).show();
                    }

                    //如果成功，则添加本地文件中个人用户的计划
					/*String filename = "yu000013@163.com.txt";
					FileOutputStream output;
					JSONObject content = new JSONObject();
					try {
						读本地文件，修改有关Plan的部分
				        content = createPlanOnFile(filename, planID, planname, begin_date, end_date, frequency, note, myfamilies);

						写本地文件
						output = myContext.openFileOutput(filename, Context.MODE_PRIVATE);
						output.write(content.toString().getBytes());  //将String字符串以字节流的形式写入到输出流中
				        output.close();         //关闭输出流
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e){
						e.printStackTrace();
					} */
                    msg="success!";

                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }

	/*private JSONObject createPlanOnFile(String filename, int planID, String planname, String begin_date, String end_date, int frequency, String note, Set<Integer> myfamilies){
		List<JSONObject> plans = new ArrayList<JSONObject>();
		FileInputStream input;
		JSONObject content = new JSONObject();
		try {
			input = myContext.openFileInput(filename);

			int length;
			length = input.available();
			byte[] temp = new byte[length];
			StringBuilder sb = new StringBuilder("");
			int len = 0;
			while ((len = input.read(temp)) > 0) {
				sb.append(new String(temp, 0, len));
			}
			//getcontent是把content变成json格式(因为是以json格式存的)
			content = JSONObject.fromObject(sb.toString());
			plans = content.getJSONArray("plans");
			//新建计划
			JSONObject newPlan = new JSONObject();
			newPlan.put("planID", planID);
			newPlan.put("planname", planname);
			newPlan.put("begin_date", begin_date);
			newPlan.put("end_date", end_date);
			newPlan.put("frequency", frequency);
			newPlan.put("note", note);
			newPlan.put("visiblefamilies", myfamilies);
			plans.add(newPlan);
			//content.remove("plans");
			content.put("plans", plans);
			return content;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return content;
	}*/
}
