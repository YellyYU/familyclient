package com.familyclient.com.familyclient.activity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.familyclient.R;
import com.familyclient.com.familyclient.action.CancelPlanAction;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ShowPlanActivity extends Activity {
    private Context mContext;
    private int planID;
    private String username;
    private SQLHelper helper;
    private SQLiteDatabase db;

    //对应的所有控件
    private TextView user, plan_title, plan_content , duration, frequency, visiblefamilies;
    private String txt_plan_title, txt_plan_content , txt_begin_date, txt_end_date, txt_frequency, txt_visiblefamilies;
    private Button btn_back, btn_edit, btn_delete;
    private ProgressBar progress;
    //private int begin_year, begin_month, begin_day, end_year, end_month, end_day;
    int total_done = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_plan);
        mContext = getApplicationContext();
        MyApp User = ((MyApp) mContext);
        username = User.getUser();
        String dbname = username+".db";
        helper = new SQLHelper(mContext, dbname, null, SQLHelper.currVer);
        db = helper.getWritableDatabase();

        plan_title = (TextView) findViewById(R.id.show_plan_title);
        plan_content = (TextView) findViewById(R.id.show_plan_content);
        duration = (TextView) findViewById(R.id.show_plan_duration);
        frequency = (TextView) findViewById(R.id.show_plan_frequency);
        visiblefamilies = (TextView) findViewById(R.id.show_plan_families);
        progress = (ProgressBar) findViewById(R.id.show_plan_progress);

        //新页面接收数据
        Bundle bundle = this.getIntent().getExtras();
        //接收planID值
        planID = bundle.getInt("planID");

        db.beginTransaction();
        Cursor cursor = db.query("Plan", null, "planID = ?", new String[]{planID+""}, null, null, null);

        List<String> visibleNames = new ArrayList<String>();
        if (cursor.moveToFirst()) {
            txt_plan_title = cursor.getString(cursor.getColumnIndex("planname"));
            plan_title.setText(txt_plan_title);
            txt_plan_content = cursor.getString(cursor.getColumnIndex("note"));
            plan_content.setText(txt_plan_content);
            txt_begin_date = cursor.getString(cursor.getColumnIndex("begin_date"));
            txt_end_date = cursor.getString(cursor.getColumnIndex("end_date"));
            duration.setText(txt_begin_date+ " 到 " + txt_end_date);
            txt_frequency = cursor.getString(cursor.getColumnIndex("frequency"));
            total_done = cursor.getInt(cursor.getColumnIndex("total_done"));
        }
        Cursor cursor1 = db.query("plan_families", null, "planID = ?", new String[]{planID+""}, null, null, null);
        if (cursor1.moveToFirst()) {
            do {
                String familyID = cursor1.getString(cursor1.getColumnIndex("familyID"));
                Cursor cursor2 = db.query("familygroup", null, "familyID = ?", new String[]{familyID+""}, null, null, null);
                if (cursor2.moveToFirst()) {
                    visibleNames.add(cursor2.getString(cursor2.getColumnIndex("family_name")));
                }
            } while (cursor1.moveToNext());
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        if(txt_frequency.equals("1")){
            frequency.setText("一天一次");
        }
        if(txt_frequency.equals("2")){
            frequency.setText("两天一次");
        }
        if(txt_frequency.equals("3")){
            frequency.setText("三天一次");
        }
        if(txt_frequency.equals("7")){
            frequency.setText("一周一次");
        }

        String makeVisibleNames = "可见家庭组：";
        for(int i=0; i<visibleNames.size(); i++){
            makeVisibleNames+="\n"+visibleNames.get(i);
        }
        visiblefamilies.setText(makeVisibleNames);

        /*Set progress bar...*/
        SimpleDateFormat d1 = new SimpleDateFormat("yyyy-MM-dd");
        Date sDate = new Date();
        try {
            sDate = d1.parse(cursor.getString(cursor.getColumnIndex("begin_date")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date tDate = new Date();
        long diff = tDate.getTime()-sDate.getTime();
        int days = (int) (diff/(1000*60*60*24));
        progress.setMax(days);
        progress.setProgress(total_done);


		/*设置头像*/
        user = (TextView) findViewById(R.id.show_plan_username);
        user.setText(username);
        Drawable[] drawable = user.getCompoundDrawables();
        // 数组下表0~3,依次是:左上右下
        drawable[0].setBounds(0, 0, 120, 120);
        user.setCompoundDrawables(drawable[0], null, null, null);


        // buttons
        btn_back = (Button) findViewById(R.id.show_plan_back_btn);
        btn_edit = (Button) findViewById(R.id.show_plan_edit_btn);
        btn_delete = (Button) findViewById(R.id.show_plan_delete_btn);

        btn_back.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(ShowPlanActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        btn_edit.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(ShowPlanActivity.this, EditPlanActivity.class);
                //用Bundle携带数据
                Bundle bundle=new Bundle();
                //传递name参数为tinyphp
                bundle.putString("planname", txt_plan_title);
                bundle.putString("note", txt_plan_content);
                bundle.putString("start_date", txt_begin_date);
                bundle.putString("end_date", txt_end_date);
                bundle.putString("frequency", txt_frequency);
                bundle.putInt("planID", planID);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        btn_delete.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                CancelPlanAction post = new CancelPlanAction(mContext);
                String result = post.CancelPlan(planID);
                Log.i("planID", planID+"");
    			/*跳回到主页面*/
                Intent intent = new Intent();
                intent.setClass(ShowPlanActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    /*private List<String> getVisibleFamiliesNames(JSONArray familyIDs, String filename){
    	List<String> visibleNames = new ArrayList<String>();
    	FileInputStream input;
    	JSONObject content = new JSONObject();
    	List<JSONObject> families = new ArrayList<JSONObject>();
    	try {
			input = mContext.openFileInput(filename);

			int length;
			length = input.available();
			byte[] temp = new byte[length];
			StringBuilder sb = new StringBuilder("");
			int len = 0;
			while ((len = input.read(temp)) > 0) {
				sb.append(new String(temp, 0, len));
			}
			//content是把内容变成json格式(因为是以json格式存的)
			content = JSONObject.fromObject(sb.toString());
			families = content.getJSONArray("families");

			//查找对应的家庭组名字

			for(int j=0;j<familyIDs.size();j++){
				for(int i=0;i<families.size();i++){
					if(familyIDs.get(j)==families.get(i).get("familyID")){
						visibleNames.add(families.get(i).getString("familyName"));
						break;
					}
				}
			}
			return visibleNames;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

    	return visibleNames;
    }*/

    /*private JSONObject getPlan(int planID, String filename){
    	JSONObject thePlan = new JSONObject();
    	FileInputStream input;
		JSONObject content = new JSONObject();
		List<JSONObject> plans = new ArrayList<JSONObject>();
		try {
			input = mContext.openFileInput(filename);

			int length;
			length = input.available();
			byte[] temp = new byte[length];
			StringBuilder sb = new StringBuilder("");
			int len = 0;
			while ((len = input.read(temp)) > 0) {
            sb.append(new String(temp, 0, len));
			}
			//getcontent是把content变成json格式(因为是以json格式存的)
			content = JSONObject.fromObject(sb.toString());

			plans = (List<JSONObject>) content.getJSONArray("plans");
			for(int i=0; i<plans.size(); i++){
				if(plans.get(i).get("planID").equals(planID)){
					thePlan = plans.get(i);
					break;
				}
			}
			return thePlan;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

    	return thePlan;
    }*/
}
