package com.familyclient.com.familyclient.action;

import com.familyclient.com.familyclient.activity.MainActivity;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class CreateFamilyAction {
    static String msg = "";
    static RequestParams params = new RequestParams();
    static Context myContext;
    private SQLHelper helper;
    private SQLiteDatabase db;
    private String username;
    private String ipAddress;

    public CreateFamilyAction() {
    }

    public CreateFamilyAction(Context mContext) {
        super();
        this.myContext = mContext;
        MyApp User = ((MyApp) myContext);
        username = User.getUser();
        ipAddress = User.getIpAddress();
    }

    public String CreateFamily(final String familyName, final String familyPassword){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("email", username);
            params.put("familyName", familyName);
            params.put("familyPassword", familyPassword);
            params.setUseJsonStreamer(true);
            client.post("http://"+ipAddress+":8080/createFamily", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="fail!";
                    Toast.makeText(myContext, "Connecting to server failed!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);
                    Log.i("html", html);

                    JSONObject newFamily = myhtml.getJSONObject("family");
                    int familyID = newFamily.getInt("familyID");
                    JSONObject theMember = newFamily.getJSONArray("members").getJSONObject(0);

                    String dbname = username+".db";
                    helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                    db = helper.getWritableDatabase();
                    db.beginTransaction();  //开始事务
                    try {
                        ContentValues newfamily = new ContentValues();
                        newfamily.put("familyID", familyID);
                        newfamily.put("family_name", familyName);
                        db.insert("familygroup", null, newfamily);

                        String filename = "members"+familyID;
                        db.execSQL("DROP TABLE IF EXISTS "+filename);
                        db.execSQL("CREATE TABLE "+filename+"(email VARCHAR(50) PRIMARY KEY,username VARCHAR(1024), total_plan INTEGER, today_task_done INTEGER, total_punish INTEGER, total_credit INTEGER, total_attack INTEGER, available_credit INTEGER, available_punish INTEGER, week_score INTEGER)");
                        ContentValues member_insert = new ContentValues();
                        member_insert.put("email", theMember.getString("email"));
                        member_insert.put("username", theMember.getString("username"));
                        member_insert.put("total_plan", theMember.getInt("total_plan"));
                        member_insert.put("today_task_done", theMember.getInt("today_task_done"));
                        member_insert.put("total_punish", theMember.getInt("total_punish"));
                        member_insert.put("total_credit", theMember.getInt("total_credit"));
                        member_insert.put("total_attack", theMember.getInt("total_attack"));
                        member_insert.put("available_credit", theMember.getInt("available_credit"));
                        member_insert.put("available_punish", theMember.getInt("available_punish"));
                        member_insert.put("week_score", theMember.getInt("week_score"));
                        db.insert(filename, null, member_insert);
                        db.setTransactionSuccessful();  //设置事务成功完成
                    } finally {
                        db.endTransaction();    //结束事务
                    }
                    db.close();
                    msg="success!";
                    Toast.makeText(myContext, "Create family succeeded!", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent();
                    intent.setClass(myContext, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    myContext.startActivity(intent);
                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }


}
