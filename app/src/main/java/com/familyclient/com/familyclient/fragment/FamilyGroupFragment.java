package com.familyclient.com.familyclient.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.familyclient.R;
import com.familyclient.com.familyclient.action.CreditAction;
import com.familyclient.com.familyclient.action.FamilyRefreshAction;
import com.familyclient.com.familyclient.action.PunishAction;
import com.familyclient.com.familyclient.action.QuitFamilyAction;
import com.familyclient.com.familyclient.activity.CreateFamilyGroupActivity;
import com.familyclient.com.familyclient.activity.CreatePunishmentActivity;
import com.familyclient.com.familyclient.activity.JoinFamilyGroupActivity;
import com.familyclient.com.familyclient.activity.MemberPlansActivity;
import com.familyclient.com.familyclient.local.FamilyGroupViewHolder;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;

//public class MyFragment extends Fragment implements OnClickListener

public class FamilyGroupFragment extends Fragment {
    private TextView txtZQD;
    private Spinner choice;

    private List<Map<String, Object>>mData = new ArrayList<Map<String, Object>>();
    private List<String> name = new ArrayList<String>();
    private List<Integer> progress_val = new ArrayList<Integer>();
    private List<Integer> progress_max = new ArrayList<Integer>();
    private List<String> emails = new ArrayList<String>();
    private List<Integer> creditNumber = new ArrayList<Integer>();
    private List<Integer> punishNumber = new ArrayList<Integer>();
    private List<Integer> scores = new ArrayList<Integer>();

    private SQLHelper helper;
    private SQLiteDatabase db;
    private static String username;
    private static Context myContext;
    private int familyID;

    private int x = 20, y = 10;
    private boolean[][] zans=new boolean[x][y];
    private boolean[][] cais=new boolean[x][y];
    private int temp_group=0;
    private int max_finish=0;

    public FamilyGroupFragment() {
        // Required empty public constructor
    }

    public static FamilyGroupFragment newInstance(Context mContext) {

        myContext = mContext;
        MyApp User = ((MyApp) myContext);
        username = User.getUser();

        FamilyGroupFragment fragment = new FamilyGroupFragment();
        Bundle args = new Bundle();
        args.putString(username, username);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_family_group,container,false);
        String dbname = username+".db";
        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
        db = helper.getWritableDatabase();
        final MyAdapter madapter = new MyAdapter(myContext);

        ListView listView = (ListView) view.findViewById(R.id.family_group_members_list);
        listView.setAdapter(madapter);
        for(int i=0;i<4;i++)
            for(int j=0;j<10;j++){
                zans[i][j]=false;
                cais[i][j]=false;
            }

        /*设置头像*/
        txtZQD = (TextView) view.findViewById(R.id.family_group_username);
        txtZQD.setText(username);
        Drawable[] drawable = txtZQD.getCompoundDrawables();

        // 数组下表0~3,依次是:左上右下
        drawable[0].setBounds(0, 0, 120, 120);
        txtZQD.setCompoundDrawables(drawable[0], null, null, null);

        /* set join family button */
        Button join_btn = (Button) view.findViewById(R.id.family_group_join_btn);
        join_btn.setOnClickListener(new View.OnClickListener(){
            public void  onClick(View v){
                Intent intent = new Intent();
                intent.setClass(myContext, JoinFamilyGroupActivity.class);
                startActivity(intent);
            }
        });

        /* set create family button */
        Button create_btn = (Button) view.findViewById(R.id.family_group_create_btn);
        create_btn.setOnClickListener(new View.OnClickListener(){
            public void  onClick(View v){
                Intent intent = new Intent();
                intent.setClass(myContext, CreateFamilyGroupActivity.class);
                startActivity(intent);
            }
        });

        /* set exit family button */
        Button exit_btn = (Button) view.findViewById(R.id.family_group_exit_btn);
        exit_btn.setOnClickListener(new View.OnClickListener(){
            public void  onClick(View v){
                QuitFamilyAction post = new QuitFamilyAction(myContext);
                String result = post.QuitFamily(familyID);
            }
        });

        /*初始化家庭组选择下拉列表*/
        choice=(Spinner) view.findViewById(R.id.family_group_choice);
        initChoice();

        choice.setOnItemSelectedListener(new OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent,View view,int position,long id){
                Spinner spinner=(Spinner)parent;
                String select=(String)spinner.getItemAtPosition(position);
                String familyid = "";
                temp_group=position;
                name.clear();
                progress_val.clear();
                progress_max.clear();
                creditNumber.clear();
                punishNumber.clear();
                scores.clear();
                emails.clear();

                for(int i=0;i<select.length();i++){
                    if(select.charAt(i)==' '){
                        break;
                    }
                    familyid+=select.charAt(i);
                }
                Log.i("familyid", familyid);
                familyID = Integer.parseInt(familyid);

        		/*拿到familyID后，发送family查看是否需要更新*/
                FamilyRefreshAction familyRefresh = new FamilyRefreshAction(myContext);
                familyRefresh.refresh(familyID);

                //是否会发生异步问题，即没来得及更新就已经读取了

        		/*更新之后，从本地读取该familyID的members*/
                db.beginTransaction();
                Cursor cursor = db.query("members"+familyID, null, null, null, null, null, null);
                if (cursor.moveToFirst()) {
                    do {
                        String username = cursor.getString(cursor.getColumnIndex("username"));
                        String today_task_done = cursor.getString(cursor.getColumnIndex("today_task_done"));
                        String total_plan = cursor.getString(cursor.getColumnIndex("total_plan"));
                        String total_punish = cursor.getString(cursor.getColumnIndex("total_punish"));
                        String total_credit = cursor.getString(cursor.getColumnIndex("total_credit"));
                        String week_score = cursor.getString(cursor.getColumnIndex("week_score"));
                        String email = cursor.getString(cursor.getColumnIndex("email"));
                        if(Integer.parseInt(week_score) > max_finish)
                            max_finish = Integer.parseInt(week_score);
                        creditNumber.add(Integer.parseInt(total_credit));
                        punishNumber.add(Integer.parseInt(total_punish));
                        scores.add(Integer.parseInt(week_score));
                        name.add(username);
                        emails.add(email);
                        progress_val.add(Integer.parseInt(today_task_done));
                        progress_max.add(Integer.parseInt(total_plan));
                    } while (cursor.moveToNext());
                }
                db.setTransactionSuccessful();
                db.endTransaction();

                mData = getData(name, progress_val, progress_max, creditNumber, punishNumber, scores, max_finish);
                madapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        return view;
    }

    /*初始化frequency下拉列表*/
    private void initChoice(){
        List<String> items = getSelectItems();
        //1.上下文 2.列表项的资源ID 3.项资源
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(myContext,R.layout.family_group_spinner_item,items);
        choice.setAdapter(adapter);

    }

    /*辅助初始化下拉列表*/
    private List<String> getSelectItems(){
        List<String> result = new ArrayList<String>();
        db.beginTransaction();
        Cursor cursor = db.query("familygroup", null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                int familyID = cursor.getInt(cursor.getColumnIndex("familyID"));
                String family_name = cursor.getString(cursor.getColumnIndex("family_name"));
                result.add(familyID+" "+family_name);
            } while (cursor.moveToNext());
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return result;
    }

    //获取动态数组数据  可以由其他地方传来(json等)
    private List<Map<String, Object>> getData(List<String> name,List<Integer> progress_val, List<Integer> progress_max,
                                              List<Integer> creditNumber, List<Integer> punishNumber, List<Integer> scores, int max_finish) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        for(int i=0;i<name.size();i++){
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("name", name.get(i));
            map.put("progress_val", progress_val.get(i));
            map.put("progress_max", progress_max.get(i));
            map.put("creditNumber", creditNumber.get(i));
            map.put("punishNumber", punishNumber.get(i));
            map.put("scores", scores.get(i));
            map.put("max_finish", max_finish);
            list.add(map);
        }
        return list;
    }

    public class MyAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public MyAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return mData.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @SuppressLint("ViewHolder")
        public View getView(int position, View convertView, ViewGroup parent) {
            FamilyGroupViewHolder holder = null;
            holder=new FamilyGroupViewHolder();//}
            convertView = mInflater.inflate(R.layout.family_group_member, null);
            holder.name = (TextView)convertView.findViewById(R.id.members_name);
            holder.cai = (Button)convertView.findViewById(R.id.members_cai);
            holder.zan=(Button)convertView.findViewById(R.id.members_zan);
            holder.create_attack=(Button)convertView.findViewById(R.id.members_attack);
            holder.score=(ProgressBar)convertView.findViewById(R.id.member_progress);
            if(zans[temp_group][position]) {holder.zan.setBackgroundResource(R.drawable.flower);}
            if(cais[temp_group][position]) {holder.cai.setBackgroundResource(R.drawable.poop);}

            CaiListener caiListener=new CaiListener(position,holder);
            ZanListener zanListener=new ZanListener(position,holder);
            NameListener nameListener=new NameListener(position,holder);
            AttackListener attackListener = new AttackListener(position, holder);
            holder.cai.setOnClickListener(caiListener);
            holder.zan.setOnClickListener(zanListener);
            holder.create_attack.setOnClickListener(attackListener);

            holder.name.setText((String)mData.get(position).get("name"));
            holder.name.setOnClickListener(nameListener);
//
//            holder.progress.setText((String)mData.get(position).get("progress"));
//            holder.cai_number.setText(((Integer)mData.get(position).get("punishNumber")).toString());
//            holder.zan_number.setText(((Integer)mData.get(position).get("creditNumber")).toString());
            holder.score.setMax((Integer)mData.get(position).get("max_finish"));
            Log.i("max",(Integer)mData.get(position).get("max_finish")+"" );
            holder.score.setProgress((Integer)mData.get(position).get("scores"));
            Log.i("score",(Integer)mData.get(position).get("scores")+"" );

            return convertView;
        }

        private class NameListener implements OnClickListener{
            int mPosition;
            FamilyGroupViewHolder mHolder;
            public NameListener(int inPosition,FamilyGroupViewHolder holder){
                mPosition= inPosition;
                mHolder=holder;
            }
            @Override
            public void onClick(View v) {
                //get the object email
                String objectEmail = emails.get(mPosition);
                Intent intent = new Intent();
                intent.putExtra("memberEmail", objectEmail);
                intent.putExtra("familyID", familyID);
                intent.putExtra("progress_val", progress_val.get(mPosition));
                intent.putExtra("progress_max", progress_max.get(mPosition));
                intent.putExtra("creditNumber", creditNumber.get(mPosition));
                intent.putExtra("punishNumber", punishNumber.get(mPosition));
                intent.setClass(myContext, MemberPlansActivity.class);
                startActivity(intent);
            }
        }

        private class CaiListener implements OnClickListener{
            int mPosition;
            FamilyGroupViewHolder mHolder;
            public CaiListener(int inPosition,FamilyGroupViewHolder holder){
                mPosition= inPosition;
                mHolder=holder;
            }
            @Override
            public void onClick(View v) {
                Toast.makeText(myContext, name.get(mPosition), Toast.LENGTH_SHORT).show();
                //get the object email
                String objectEmail = emails.get(mPosition);
                Button cai=(Button)v;
                cai.setBackgroundResource(R.drawable.poop);
                cais[temp_group][mPosition]=true;

                PunishAction post = new PunishAction(myContext);
                post.Punish(username, objectEmail, familyID);

                cai.setClickable(false);
            }

        }

        private class ZanListener implements OnClickListener{
            int mPosition;
            FamilyGroupViewHolder mHolder;
            public ZanListener(int inPosition,FamilyGroupViewHolder holder){
                mPosition= inPosition;
                mHolder=holder;
            }
            @Override
            public void onClick(View v) {
                Toast.makeText(myContext, name.get(mPosition), Toast.LENGTH_SHORT).show();
                //get the object email
                String objectEmail = emails.get(mPosition);
                Button zan=(Button)v;
                zan.setBackgroundResource(R.drawable.flower);
                //mHolder.doZan=true;
                //view_holder_list.set(mPosition, mHolder);
                zans[temp_group][mPosition]=true;
				/*int tem = creditNumber.get(mPosition);
				creditNumber.set(mPosition, tem+1);*/

                CreditAction post = new CreditAction(myContext);
                post.Credit(username, objectEmail, familyID);

                zan.setClickable(false);
            }
        }

        private class AttackListener implements OnClickListener{
            int mPosition;
            FamilyGroupViewHolder mHolder;
            public AttackListener(int inPosition,FamilyGroupViewHolder holder){
                mPosition= inPosition;
                mHolder=holder;
            }
            @Override
            public void onClick(View v) {
                String objectEmail = emails.get(mPosition);
                boolean flag = LesserThanFive(objectEmail);

                if(username.equals(objectEmail)){
                    Toast.makeText(myContext, "不能攻击自己哦!" , Toast.LENGTH_SHORT).show();
                }
                else if(flag){
                    Toast.makeText(myContext, "对方的可用被踩数少于5，不能发起攻击!" , Toast.LENGTH_SHORT).show();
                }
                else{
                    Intent intent = new Intent();
                    intent.putExtra("receiver", objectEmail);
                    intent.putExtra("familyID", familyID);
                    intent.setClass(myContext, CreatePunishmentActivity.class);
                    startActivity(intent);
                }
            }
        }

    }

    private boolean LesserThanFive(String objectEmail){
        boolean flag = false;

        String dbname = username+".db";
        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
        db = helper.getWritableDatabase();
        db.beginTransaction();  //开始事务
        try {
            String tablename = "members"+familyID;
            Log.i("tablename", tablename);
            Cursor cursor = db.query(tablename, null, "email = ?", new String[]{objectEmail}, null, null,null);

            int available_punish = 0;
            if (cursor.moveToFirst()) {
                available_punish = cursor.getInt(cursor.getColumnIndex("available_punish"));
            }
            Log.i("available_punish", available_punish+"");
            if(available_punish<5){
                //Toast.makeText(myContext, "对方的被踩数少于5，不能发起攻击!" , Toast.LENGTH_SHORT).show();
                flag = true;
            }
            db.setTransactionSuccessful();  //设置事务成功完成
        } finally {
            db.endTransaction();    //结束事务
        }
        //db.close();
        return flag;
    }
}


