package com.familyclient.com.familyclient.action;

import java.util.List;

import com.familyclient.com.familyclient.activity.MainActivity;
import com.familyclient.com.familyclient.local.MyApp;
import com.familyclient.com.familyclient.local.SQLHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;
import net.sf.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class LoginAction {
    static String msg = "";
    static RequestParams params = new RequestParams();
    static Context myContext;
    private SQLHelper helper;
    private SQLiteDatabase db;
    private String ipAddress;

    public LoginAction() {
    }

    public LoginAction(Context mContext) {
        super();
        this.myContext = mContext;
    }

    public String Auth(final String email, final String password){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            params.put("email", email);
            params.put("pwd", password);
            params.setUseJsonStreamer(true);
            ipAddress = "192.168.10.153";

            client.post("http://"+ipAddress+":8080/auth", params, new AsyncHttpResponseHandler(){
                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                    msg="连接服务器失败!";
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] arg2){
                    String html = new String(arg2);
                    JSONObject myhtml = JSONObject.fromObject(html);

                    int flag = myhtml.getInt("flag");
                    String message = myhtml.getString("message");
                    JSONObject user = myhtml.getJSONObject("user");
                    List<JSONObject> plans = myhtml.getJSONArray("plans");
                    List<JSONObject> families = myhtml.getJSONArray("families");


                    //如果成功，则更新本地文件中个人用户的计划
                    if(flag == 1){
                        MyApp User = ((MyApp) myContext);
                        User.setUser(email);
                        User.setFamilyAmount(families.size());
                        User.setIpAddress(ipAddress);

                        String dbname = email+".db";
                        helper = new SQLHelper(myContext, dbname, null, SQLHelper.currVer);
                        db = helper.getWritableDatabase();

                        db.beginTransaction();  //开始事务
                        try {
                            ContentValues newuser = new ContentValues();
                            newuser.put("email", user.getString("email"));
                            newuser.put("username", user.getString("username"));
                            newuser.put("password", user.getString("password"));
                            db.replace("User", null, newuser);


                            JSONObject thePlan = new JSONObject();
                            db.delete("plan_families", null, null);
                            db.delete("Plan", null, null);
                            for(int i=0;i<plans.size();i++){
                                ContentValues plan_insert = new ContentValues();
                                thePlan = plans.get(i);
                                plan_insert.put("planID", thePlan.getInt("planID"));
                                plan_insert.put("planname", thePlan.getString("planname"));
                                plan_insert.put("frequency", thePlan.getInt("frequency"));
                                plan_insert.put("note", thePlan.getString("note"));
                                plan_insert.put("begin_date", thePlan.getString("begin_date"));
                                plan_insert.put("end_date", thePlan.getString("end_date"));
                                plan_insert.put("total_done", thePlan.getString("total_done"));
                                plan_insert.put("today_done", thePlan.getString("today_done"));
                                db.replace("Plan", null, plan_insert);

                                List<Integer> visiblefamilies = thePlan.getJSONArray("visiblefamilies");
                                for(int j = 0;j<visiblefamilies.size();j++){
                                    ContentValues plan_families_insert = new ContentValues();
                                    plan_families_insert.put("planID",thePlan.getInt("planID"));
                                    plan_families_insert.put("familyID", visiblefamilies.get(j));
                                    db.replace("plan_families", null, plan_families_insert);
                                }
                            }

                            JSONObject theFamily = new JSONObject();
                            for(int i=0;i<families.size();i++){
                                ContentValues family_insert = new ContentValues();
                                theFamily = families.get(i);
                                List<JSONObject> members = theFamily.getJSONArray("members");
                                family_insert.put("familyID", theFamily.getInt("familyID"));
                                family_insert.put("family_name", theFamily.getString("familyName"));
                                db.replace("familygroup", null, family_insert);

                                String filename = "members"+theFamily.getInt("familyID");
                                db.execSQL("DROP TABLE IF EXISTS "+filename);
                                db.execSQL("CREATE TABLE "+filename+"(email VARCHAR(50) PRIMARY KEY,username VARCHAR(1024), total_plan INTEGER, today_task_done INTEGER, total_punish INTEGER, total_credit INTEGER, total_attack INTEGER, available_credit INTEGER, available_punish INTEGER, week_score INTEGER)");

                                for(int j=0;j<members.size();j++){
                                    ContentValues member_insert = new ContentValues();
                                    JSONObject theMember = members.get(j);
                                    member_insert.put("email", theMember.getString("email"));
                                    member_insert.put("username", theMember.getString("username"));
                                    member_insert.put("total_plan", theMember.getInt("total_plan"));
                                    member_insert.put("today_task_done", theMember.getInt("today_task_done"));
                                    member_insert.put("total_punish", theMember.getInt("total_punish"));
                                    member_insert.put("total_credit", theMember.getInt("total_credit"));
                                    member_insert.put("total_attack", theMember.getInt("total_attack"));
                                    member_insert.put("available_credit", theMember.getInt("available_credit"));
                                    member_insert.put("available_punish", theMember.getInt("available_punish"));
                                    member_insert.put("week_score", theMember.getInt("week_score"));
                                    db.replace(filename, null, member_insert);
                                }
                            }

                            db.setTransactionSuccessful();  //设置事务成功完成
                        } finally {
                            db.endTransaction();    //结束事务
                        }
                        db.close();

                        msg = "Login succeeded!";
                        //Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                        Toast.makeText(myContext, "登录成功", Toast.LENGTH_SHORT).show();

                        MyApp theUser = ((MyApp) myContext);
                        String username = theUser.getUser();
                        Log.i("username", username);

                        Intent intent = new Intent();
                        intent.setClass(myContext, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        myContext.startActivity(intent);

                    }
                    if(flag == 0){
                        Toast.makeText(myContext, "用户名/密码错误", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch(Exception e){e.printStackTrace();}
        return msg;
    }
}
