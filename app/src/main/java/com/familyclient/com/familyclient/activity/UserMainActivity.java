package com.familyclient.com.familyclient.activity;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.familyclient.R;

import net.sf.json.JSONObject;

public class UserMainActivity extends Activity {
    private ListView myList;
    private Context myContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_user_main);
        myContext = getApplicationContext();
        myList=(ListView) findViewById(R.id.user_main_lists);
        //String[] array={"我吃了羊蝎子","我吃了酸奶冰激凌","我吃了红柳羊肉串","1","2","3","4","5","6","7"};
        String filename = "yu000013@163.com.credit&punish.txt";
        List<String> list = log(filename);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,R.layout.user_main_list_item,list);
        myList.setAdapter(adapter);

	    /**/
        Button quit=(Button)findViewById(R.id.user_main_quit);
        quit.setOnClickListener(new View.OnClickListener(){
            public void  onClick(View v){
				/*点击时注销当前账户，然后回到登录页面*/
                Intent intent = new Intent();
                intent.setClass(UserMainActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private List<String> log(String filename){
        List<String> result = new ArrayList<String>();
        FileInputStream input;
        try {
            input = myContext.openFileInput(filename);
            //用bufferedReader按行读取
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input));

            String str = null;
            while((str = bufferedReader.readLine()) != null)
            {
                result.add(str);
            }
            Log.i("size", result.size()+"");
            return result;


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
