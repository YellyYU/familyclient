package com.familyclient.com.familyclient.local;

import android.app.Application;

public class MyApp extends Application {
    private String username;
    private int familyAmount;
    private String ipAddress;

    public String getUser(){
        return username;
    }
    public void setUser(String s){
        username = s;
    }

    public String getIpAddress(){
        return ipAddress;
    }
    public void setIpAddress(String s){
        ipAddress = s;
    }

    public int getFamilyAmount(){
        return familyAmount;
    }

    public void setFamilyAmount(int i){
        familyAmount = i;
    }
}
